﻿using log4net.Config;
using ServiceReference1;
using System;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;

namespace _2H
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            Program obj = new Program();
            Console.WriteLine("Please Enter Input values..");
            string FromId, FromSystemId, ToId, ToSystemId;
            Console.Write("FromId: ");
            FromId = Console.ReadLine();
            Console.Write("FromSystemId: ");
            FromSystemId = Console.ReadLine();
            Console.Write("ToId: ");
            ToId = Console.ReadLine();
            Console.Write("ToSystemId: ");
            ToSystemId = Console.ReadLine();
            Console.Write("TasksetUId: ");
            long TasksetUId = Convert.ToInt64(Console.ReadLine());
            TasksetFetchResponse1 result;

            var endpoint = new EndpointAddress(new Uri("https://test-b2b1.reggefiber.nl/bp/ws/taskhandling/v20140701/service.wsdl"));
            var client = new getConnectionInfoClient(new BasicHttpBinding(BasicHttpSecurityMode.Transport), endpoint);
            client.ChannelFactory.Credentials.ServiceCertificate.SslCertificateAuthentication = new X509ServiceCertificateAuthentication
            {
                CertificateValidationMode = X509CertificateValidationMode.None,
                RevocationMode = X509RevocationMode.NoCheck
            };
            client.ChannelFactory.Credentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
            try
            {
                using (new OperationContextScope(client.InnerChannel))
                {
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    var cridential = Base64Encode($"{"GNC_SYS1"}:{"wG4h2!h"}");
                    requestMessage.Headers["Authorization"] = $"Basic {cridential}";
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;
                    var tasksetRequest = new TasksetFetchRequest
                    {
                        header = new Header
                        {
                            from = new Party
                            {
                                id = /*"GNC"*/  FromId,
                                systemId = /*"GNC_SYS1"*/  FromSystemId
                            },
                            to = new Party
                            {
                                id = /*"GNC"*/  ToId,
                                systemId = /*"GNC_SYS1"*/  ToSystemId
                            }
                        },
                        tasksetUid = new long[] { /*15606478*/ TasksetUId }
                    };
                    var response = client.TasksetFetchAsync(tasksetRequest);
                    result = response.Result;
                }
                if (result != null)
                {
                    log.Info(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    Console.WriteLine("Complete");
                }

            }
            catch (Exception e)
            {
                //_logger.LogException(CpLogging.CWH0007, e);
                log.Error(e);
                throw;
            }

            Console.ReadKey();
        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}
