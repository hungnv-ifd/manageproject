namespace ManageProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class database : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.APIs",
                c => new
                    {
                        API = c.String(nullable: false, maxLength: 128),
                        InfoAPI = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Type = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.API);
            
            CreateTable(
                "dbo.ProjectEnvironments",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Project = c.String(maxLength: 128),
                        Environment = c.String(maxLength: 128),
                        IdAPI = c.String(maxLength: 128),
                        IdProtocol = c.String(maxLength: 128),
                        IdDatabase = c.String(maxLength: 128),
                        IdWebsite = c.String(maxLength: 128),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.APIs", t => t.IdAPI)
                .ForeignKey("dbo.Databases", t => t.IdDatabase)
                .ForeignKey("dbo.Enviroments", t => t.Environment)
                .ForeignKey("dbo.ManageProjects", t => t.Project)
                .ForeignKey("dbo.Protocols", t => t.IdProtocol)
                .ForeignKey("dbo.Websites", t => t.IdWebsite)
                .Index(t => t.Project)
                .Index(t => t.Environment)
                .Index(t => t.IdAPI)
                .Index(t => t.IdProtocol)
                .Index(t => t.IdDatabase)
                .Index(t => t.IdWebsite);
            
            CreateTable(
                "dbo.Databases",
                c => new
                    {
                        Database = c.String(nullable: false, maxLength: 128),
                        InstanceName = c.String(),
                        DatabaseName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Database);
            
            CreateTable(
                "dbo.Enviroments",
                c => new
                    {
                        Environment = c.String(nullable: false, maxLength: 128),
                        Type = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Environment);
            
            CreateTable(
                "dbo.ServerEnvironments",
                c => new
                    {
                        Environment = c.String(nullable: false, maxLength: 128),
                        Server = c.String(nullable: false, maxLength: 128),
                        Type = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Environment, t.Server })
                .ForeignKey("dbo.Enviroments", t => t.Environment, cascadeDelete: true)
                .ForeignKey("dbo.Servers", t => t.Server, cascadeDelete: true)
                .Index(t => t.Environment)
                .Index(t => t.Server);
            
            CreateTable(
                "dbo.Servers",
                c => new
                    {
                        Server = c.String(nullable: false, maxLength: 128),
                        ServerName = c.String(),
                        Type = c.String(),
                        Location = c.String(),
                        InfoServer = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Server);
            
            CreateTable(
                "dbo.ManageProjects",
                c => new
                    {
                        Project = c.String(nullable: false, maxLength: 128),
                        ProjectName = c.String(nullable: false),
                        Summary = c.String(),
                        LanguageCode = c.String(nullable: false),
                        Tech = c.String(nullable: false),
                        removeServer = c.String(),
                        isJoin = c.String(nullable: false),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                        DateDeployment = c.DateTime(nullable: false),
                        InfoFlyway = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Project);
            
            CreateTable(
                "dbo.Bitbuckets",
                c => new
                    {
                        Bitbucket = c.String(nullable: false, maxLength: 128),
                        Project = c.String(maxLength: 128),
                        Link = c.String(),
                        InfoBitbucket = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Bitbucket)
                .ForeignKey("dbo.ManageProjects", t => t.Project)
                .Index(t => t.Project);
            
            CreateTable(
                "dbo.UserProjects",
                c => new
                    {
                        Project = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Disable = c.Boolean(nullable: false),
                        TaskUser = c.String(nullable: false),
                        PositionUser = c.String(nullable: false),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Project, t.UserId })
                .ForeignKey("dbo.ManageProjects", t => t.Project, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.Project)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(),
                        Phone = c.String(),
                        SkypeAccount = c.String(),
                        BitbucketAccount = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Protocols",
                c => new
                    {
                        Protocol = c.String(nullable: false, maxLength: 128),
                        HostName = c.String(),
                        Port = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Notes = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Protocol);
            
            CreateTable(
                "dbo.Websites",
                c => new
                    {
                        Website = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        InfoWebsite = c.String(),
                        Updatedate = c.DateTime(),
                        Updateby = c.String(),
                        Createdate = c.DateTime(),
                        Createby = c.String(),
                        Deletedate = c.DateTime(),
                        Deleteby = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Website);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        IdEmail = c.String(nullable: false, maxLength: 128),
                        Type = c.String(),
                        Description = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                    })
                .PrimaryKey(t => t.IdEmail);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ProjectEnvironments", "IdWebsite", "dbo.Websites");
            DropForeignKey("dbo.ProjectEnvironments", "IdProtocol", "dbo.Protocols");
            DropForeignKey("dbo.UserProjects", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserProjects", "Project", "dbo.ManageProjects");
            DropForeignKey("dbo.ProjectEnvironments", "Project", "dbo.ManageProjects");
            DropForeignKey("dbo.Bitbuckets", "Project", "dbo.ManageProjects");
            DropForeignKey("dbo.ServerEnvironments", "Server", "dbo.Servers");
            DropForeignKey("dbo.ServerEnvironments", "Environment", "dbo.Enviroments");
            DropForeignKey("dbo.ProjectEnvironments", "Environment", "dbo.Enviroments");
            DropForeignKey("dbo.ProjectEnvironments", "IdDatabase", "dbo.Databases");
            DropForeignKey("dbo.ProjectEnvironments", "IdAPI", "dbo.APIs");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.UserProjects", new[] { "UserId" });
            DropIndex("dbo.UserProjects", new[] { "Project" });
            DropIndex("dbo.Bitbuckets", new[] { "Project" });
            DropIndex("dbo.ServerEnvironments", new[] { "Server" });
            DropIndex("dbo.ServerEnvironments", new[] { "Environment" });
            DropIndex("dbo.ProjectEnvironments", new[] { "IdWebsite" });
            DropIndex("dbo.ProjectEnvironments", new[] { "IdDatabase" });
            DropIndex("dbo.ProjectEnvironments", new[] { "IdProtocol" });
            DropIndex("dbo.ProjectEnvironments", new[] { "IdAPI" });
            DropIndex("dbo.ProjectEnvironments", new[] { "Environment" });
            DropIndex("dbo.ProjectEnvironments", new[] { "Project" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Emails");
            DropTable("dbo.Websites");
            DropTable("dbo.Protocols");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.UserProjects");
            DropTable("dbo.Bitbuckets");
            DropTable("dbo.ManageProjects");
            DropTable("dbo.Servers");
            DropTable("dbo.ServerEnvironments");
            DropTable("dbo.Enviroments");
            DropTable("dbo.Databases");
            DropTable("dbo.ProjectEnvironments");
            DropTable("dbo.APIs");
        }
    }
}
