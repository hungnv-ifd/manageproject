﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ManageProject.BuilderDesignPattern
{
    public class Email
    {
        [Key]
        public String IdEmail { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
       
      
        
    }
}