﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageProject.Models
{
    public class UserProject
    {
        [Key]
        [Column(Order = 1)]
        public string Project { get; set; }
        [ForeignKey("Project")]
        public virtual ManageProjects managerProject { get; set; }

        [Key]
        [Column(Order = 2)]
        public string UserId { get; set; }
        public bool Disable { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser manageUser { get; set; }
        [Required]
        public string TaskUser { get; set; }
        [Required]
        public string PositionUser { get; set; }

        public string Notes { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }

    }
}