﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageProject.Models
{
    public class ServerEnvironments
    {
        [Key, Column(Order = 0)]
        public string Environment { get; set; }
        [ForeignKey("Environment")]
        public virtual Enviroment Environments { get; set; }

        [Key, Column(Order = 1)]
        public string Server { get; set; }
        [ForeignKey("Server")]
        public virtual Servers Servers { get; set; }

        public string Type { get; set; }
        public string Notes { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }
    }
}