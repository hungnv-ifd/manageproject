﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageProject.Models
{
    public class ManageProjects
    {
        [Key, Column(Order = 0)]
        public string Project { get; set; }

        [Required(ErrorMessage = "Value is not empty ")]
        public string ProjectName { get; set; }
        public string Summary { get; set; }
        [Required(ErrorMessage = "Value is not empty ")]
        public string LanguageCode { get; set; }
        [Required(ErrorMessage = "Value is not empty ")]
        public string Tech { get; set; }
        public string removeServer { get; set; }
        [Display(Name ="Status")]
        [Required(ErrorMessage = "Value is not empty ")]
        public string isJoin { get; set; }

        public string Notes { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }
        
        //[Required]
        //public string InfoDb { get; set; }
        //[Required]
        //public string InfoAPI { get; set; }

        public DateTime DateDeployment { get; set; }
        [Required(ErrorMessage = "Value is not empty ")]
        public string InfoFlyway { get; set; }

        public virtual ICollection<UserProject> UserProjects { get; set; }
        public virtual ICollection<Bitbuckets> Bitbuckets { get; set; }
        public virtual ICollection<ProjectEnvironments> ProjectEnvironments { get; set; }
        

    }
}