﻿using ManageProject.BuilderDesignPattern;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManageProject.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string SkypeAccount { get; set; }
        public string BitbucketAccount { get; set; }

        public string Notes { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }
        public virtual ICollection<UserProject> UserProjects { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    
   
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public System.Data.Entity.DbSet<ManageProjects> ManageProjects { get; set; }
        public System.Data.Entity.DbSet<UserProject> UserProjects { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<APIs> APIs { get; set; }

        public System.Data.Entity.DbSet<Enviroment> Enviroments { get; set; }

        public System.Data.Entity.DbSet<Databases> Databases { get; set; }

        public System.Data.Entity.DbSet<Bitbuckets> Bitbuckets { get; set; }

        public System.Data.Entity.DbSet<Websites> Websites { get; set; }

        public System.Data.Entity.DbSet<Protocols> Protocols { get; set; }

        public System.Data.Entity.DbSet<Servers> Servers { get; set; }
        public System.Data.Entity.DbSet<Email> Emails { get; set; }

        public System.Data.Entity.DbSet<ManageProject.Models.ServerEnvironments> ServerEnvironments { get; set; }
        public System.Data.Entity.DbSet<ProjectEnvironments> ProjectEnvironments { get; set; }
        public IObjectSet<object> CreateObjectSet { get; internal set; }
    }
}