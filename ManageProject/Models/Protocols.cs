﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageProject.Models
{
    public class Protocols
    {
        public Protocols()
        {
            ProjectEnvironments = new List<ProjectEnvironments>();
        }
        [Key]
        public string Protocol { get; set; }
        public string HostName { get; set; }

        public string Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Notes { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }
        public string Type { get; set; }

        public virtual ICollection<ProjectEnvironments> ProjectEnvironments { get; set; }
    }
}