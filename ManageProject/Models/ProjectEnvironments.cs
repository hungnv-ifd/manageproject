﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageProject.Models
{
    public class ProjectEnvironments
    {
        [Key]
        public string Id { get; set; }
        public string Project { get; set; }
        [ForeignKey("Project")]
        public virtual ManageProjects Projects { get; set; }
        public string Environment { get; set; }
        [ForeignKey("Environment")]
        public virtual Enviroment Environments { get; set; }
        public string IdAPI { get; set; }
        [ForeignKey("IdAPI")]
        public virtual APIs APIs { get; set; }
        public string IdProtocol { get; set; }
        [ForeignKey("IdProtocol")]
        public virtual Protocols Protocols { get; set; }
        public string IdDatabase { get; set; }
        [ForeignKey("IdDatabase")]
        public virtual Databases Databases { get; set; }
        public string IdWebsite { get; set; }
        [ForeignKey("IdWebsite")]
        public virtual Websites Websites { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }
    }
}