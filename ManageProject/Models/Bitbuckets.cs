﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ManageProject.Models
{
    public class Bitbuckets
    {

        [Key]
        public string Bitbucket { get; set; }

        public string Project { get; set; }
        [ForeignKey("Project")]
        public virtual ManageProjects Projects { get; set; }

        public string Link { get; set; }

        public string InfoBitbucket { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createby { get; set; }
        public DateTime? Deletedate { get; set; }
        public string Deleteby { get; set; }
        public bool IsDelete { get; set; }

    }
}