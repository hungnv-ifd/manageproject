﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageProject.BuilderDesignPattern
{
    class SendPassword : EmailBuilder
    {
        public override String buildBody(string body)
        {
            email.Body = body;
            return body;
        }

        public override void buildDescription(string description)
        {
            email.Description = description;
        }

        public override void buildSubject(string subject)
        {
            email.Subject = subject;
        }

        public override void buildType(string type)
        {
            email.Type = type;
        }
    }
}