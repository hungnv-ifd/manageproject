﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageProject.BuilderDesignPattern
{
    class EmailDirector
    {
        private EmailBuilder emailBuilder;
        public void setEmailBuilder(EmailBuilder e)
        {
            this.emailBuilder = e;
        }
        public Email getEmail()
        {
            return emailBuilder.getEmail();
        }
        public void constructorEmailDirection(string type,string description,string subject,string body)
        {

            emailBuilder.createNewEmail();
            emailBuilder.build();
          
            emailBuilder.buildType(type);
            emailBuilder.buildDescription(description);
            emailBuilder.buildSubject(subject);
            emailBuilder.buildBody(body);
          
          
           
        }

    }
}