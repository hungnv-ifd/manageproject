﻿using ManageProject.Models;
using System.IO;
using System.Linq;
using Microsoft.AspNet.Identity;
using System;

namespace ManageProject.BuilderDesignPattern
{
    class ForgotPassword :EmailBuilder
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public override String buildBody(string id)
        {

            email.Body = id;
            //string body = string.Empty;
            ////using streamreader for reading my htmltemplate   
            ////using (StreamReader reader = new StreamReader(Server.MapPath("~/Views/TemplateEmail/MailProtocol.cshtml")))
            ////{

            ////    body = reader.ReadToEnd();

            ////}
            //var tmp = db.Protocols.Where(x => x.IdProtocol == id).FirstOrDefault();
            //var projectName = tmp.IdEnviroments.ProjectIds.ProjectName;

            //body = body.Replace("{FullName}", tmp.UserName);
            //body = body.Replace("{ProjectName}", projectName);
            //body = body.Replace("{HostName}", tmp.HostName); //replacing the required things  

            //body = body.Replace("{Port}", tmp.Port);

            //body = body.Replace("{Type}", tmp.Type);
            //body = body.Replace("{UserName}", tmp.UserName);
            //body = body.Replace("{Password}", tmp.Password);
            //body = body.Replace("{CreateDate}", tmp.Createdate.ToString());
            //body = body.Replace("{UpdateDate}", tmp.Updatedate.ToString());

            //email.Body = body;
            return id;
        }

        public override void buildDescription(string description)
        {
            email.Description = description;
           
        }
        public override void buildSubject(string subject)
        {
            email.Subject = subject;
        }

        public override void buildType(string type)
        {
            email.Type = type;
        }
    }
}