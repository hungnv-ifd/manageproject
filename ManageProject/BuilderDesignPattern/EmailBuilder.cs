﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ManageProject.BuilderDesignPattern
{
    abstract class EmailBuilder:Controller
    {
        protected Email email;
        public Email getEmail()
        {
            return email;
        }
        public void createNewEmail()
        {
            email=new Email();
        }

        
        public abstract void buildType(string type);
        public abstract void buildDescription(string description);
        public abstract void buildSubject(string subject);
        public abstract String buildBody(string body);
      
       

        public void build()
        {
            email.IdEmail= Guid.NewGuid().ToString();
          
        }
    }
}