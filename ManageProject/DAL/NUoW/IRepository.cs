﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ManageProject.DAL.NUoW
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        TEntity GetbyID2(object id, object id2);
        TEntity GetbyID(object id);
        void Insert(TEntity entity);
        void Delete2(object id, object id2);
        void Delete(object id);
        void Delete(TEntity entitytodelete);
        void Update(TEntity entitytoupdate);
    }
}
