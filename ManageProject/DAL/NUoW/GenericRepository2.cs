﻿using ManageProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ManageProject.DAL.NUoW
{
    public class GenericRepository2<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal ApplicationDbContext context;
        internal DbSet<TEntity> dbset;
        public GenericRepository2(ApplicationDbContext context)
        {
            this.context = context;
            this.dbset = context.Set<TEntity>();
        }
        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbset;
            
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else { return query.ToList(); }

        }
        public virtual TEntity GetbyID2(object id, object id2)
        {
            return dbset.Find(id, id2);
        }
        public virtual TEntity GetbyID(object id)
        {
            return dbset.Find(id);
        }
        public virtual void Insert(TEntity entity)
        {
            dbset.Add(entity);
        }
        public virtual void Delete2(object id, object id2)
        {
            TEntity entitytodelete2 = dbset.Find(id, id2);
            Delete(entitytodelete2);
        }
        public virtual void Delete(object id)
        {
            TEntity entitytodelete = dbset.Find(id);
            Delete(entitytodelete);
        }
        public virtual void Delete(TEntity entitytodelete)
        {
            if (context.Entry(entitytodelete).State == EntityState.Detached)
            {
                dbset.Attach(entitytodelete);
            }
            dbset.Remove(entitytodelete);
        }
        public virtual void Update(TEntity entitytoupdate)
        {
            dbset.Attach(entitytoupdate);
            context.Entry(entitytoupdate).State = EntityState.Modified;
        }
    }
}