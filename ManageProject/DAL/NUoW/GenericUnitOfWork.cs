﻿using ManageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageProject.DAL.NUoW
{
    public class GenericUnitOfWork : IDisposable
    {
        private ApplicationDbContext context;
        public GenericUnitOfWork()
        {
            context = new ApplicationDbContext();
        }
        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();
        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (repositories.Keys.Contains(typeof(TEntity)) == true)
            {
                return repositories[typeof(TEntity)] as IRepository<TEntity>;
            }
            IRepository<TEntity> repo = new GenericRepository2<TEntity>(context);
            repositories.Add(typeof(TEntity), repo);
            return repo;
        }
        public void Save()
        {
            context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}