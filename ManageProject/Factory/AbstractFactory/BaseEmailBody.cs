﻿using ManageProject.BuilderDesignPattern;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageProject.Factory.AbstractFactory
{
    abstract class BaseEmailBody
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        public virtual string Body()
        {
            return TopHTML().ToString() +
                "<th>ProjectName</th>" +
                "<th>InstanceName</th>" +
                "<th>DatabaseName</th>" +
                "<th>UserName</th>" +
                "<th>Password</th>" +
                "<th>CreateDate</th>" +
                "<th>UpdateDate</th>" +
                MidHTML().ToString() +
                "<td>{ProjectName}</td>" +
                "<td>{InstanceName}</td>" +
                "<td>{DatabaseName}</td>" +
                "<td>{UserName}</td>" +
                "<td>{Password}</td>" +
                "<td>{CreateDate}</td>" +
                "<td>{UpdateDate}</td>" + BottomHTML().ToString();
        }
        public virtual string ShowBody(string name, string objectName)
        {
            var tmp = unitOfWork.Repository<Protocols>().Get().Where(x => x.HostName == objectName && x.IsDelete == false).FirstOrDefault();
            Email e = unitOfWork.Repository<Email>().Get().Where(k => k.Type == "Protocol").SingleOrDefault();
            string body = e.Body;
            body = body.Replace("{ProjectName}", name);
            body = body.Replace("{Type}", tmp.Type);
            body = body.Replace("{HostName}", tmp.HostName);
            body = body.Replace("{Port}", tmp.Port);
            body = body.Replace("{UserName}", tmp.UserName);
            body = body.Replace("{Password}", tmp.Password);
            body = body.Replace("{CreateDate}", tmp.Createdate.ToString());
            body = body.Replace("{UpdateDate}", tmp.Updatedate.ToString());
            return body;
        }
        public virtual string ShowBody(string name, string objectName, string objectName2)
        {
            var tmp = unitOfWork.Repository<ServerEnvironments>().Get(filter: s => s.Servers.ServerName == objectName && s.Environments.Type == objectName2).FirstOrDefault();
            Email e = unitOfWork.Repository<Email>().Get().Where(k => k.Type == "Server").SingleOrDefault();

            string body = e.Body;
            body = body.Replace("{ProjectName}", name);
            body = body.Replace("{ServerName}", tmp.Servers.ServerName);
            body = body.Replace("{Environment}", tmp.Environments.Type);
            body = body.Replace("{ServerType}", tmp.Servers.Type);
            body = body.Replace("{ServerLocation}", tmp.Servers.Location);
            body = body.Replace("{ServerDescription}", tmp.Servers.InfoServer);
            body = body.Replace("{Type}", tmp.Type);
            body = body.Replace("{Notes}", tmp.Notes);
            body = body.Replace("{CreateDate}", tmp.Createdate.ToString());
            body = body.Replace("{UpdateDate}", tmp.Updatedate.ToString());
            return body;
        }

        public string TopHTML()
        {
            return "<table class=\"table table-bordered table-hover table-striped\" id=\"myTable\">" +
                "<thead class=\"\" style=\"background-color:forestgreen;color:white\"><tr>";
        }
        public string MidHTML()
        {
            return "</tr></thead><tbody><tr>";
        }
        public string BottomHTML()
        {
            return "</tr></tbody></table>";
        }

    }
    
}
