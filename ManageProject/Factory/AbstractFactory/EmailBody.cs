﻿using ManageProject.BuilderDesignPattern;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageProject.Factory.AbstractFactory
{
    class BodyDatabase : BaseEmailBody
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        public override string ShowBody(string name, string objectName)
        {

            Databases tmp = unitOfWork.Repository<Databases>().Get().Where(x => x.DatabaseName == objectName && x.IsDelete == false).SingleOrDefault();
            Email e = unitOfWork.Repository<Email>().Get().Where(k => k.Type == "Database").SingleOrDefault();
            string body = e.Body;
            body = body.Replace("{ProjectName}", name);
            body = body.Replace("{InstanceName}", tmp.InstanceName);
            body = body.Replace("{DatabaseName}", tmp.DatabaseName);
            body = body.Replace("{UserName}", tmp.UserName);
            body = body.Replace("{Password}", tmp.Password);
            body = body.Replace("{CreateDate}", tmp.Createdate.ToString());
            body = body.Replace("{UpdateDate}", tmp.Updatedate.ToString());
            return body;
        }
        
    }
    class BodyProtocol : BaseEmailBody
    {
        public override string Body()
        {
            return TopHTML().ToString() +
                "<th>ProjectName</th>" +
                "<th>Type</th>" +
                "<th>HostName</th>" +
                "<th>Port</th>" +
                "<th>UserName</th>" +
                "<th>Password</th>" +
                "<th>CreateDate</th>" +
                "<th>UpdateDate</th>" +
                MidHTML().ToString() +
                "<td>{ProjectName}</td>" +
                "<td>{Type}</td>" +
                "<td>{HostName}</td>" +
                "<td>{Port}</td>" +
                "<td>{UserName}</td>" +
                "<td>{Password}</td>" +
                "<td>{CreateDate}</td>" +
                "<td>{UpdateDate}</td>" + BottomHTML().ToString();
        }
    }
    class BodyServer : BaseEmailBody
    {
        public override string Body()
        {
            return TopHTML().ToString() +
                "<tr><th>ProjectName</th>" +
                "<th>Server Name</th>" +
                "<th>Environment</th>" +
                "<th>Server Type</th>" +
                "<th>Server Location</th>" +
                "<th>Server Description</th>" +
                "<th>Type</th>" +
                "<th>Notes</th>" +
                "<th>Create Date</th>" +
                "<th>Update Date</th>" +
                MidHTML().ToString() +
                "<td>{ProjectName}</td>" +
                "<td>{ServerName}</td>" +
                "<td>{Environment}</td>" +
                "<td>{ServerType}</td>" +
                "<td>{ServerLocation}</td>" +
                "<td>{ServerDescription}</td>" +
                "<td>{Type}</td>" +
                "<td>{Notes}</td>" +
                "<td>{CreateDate}</td>" +
                "<td>{UpdateDate}</td>" + BottomHTML().ToString();
        }
        
    }
}