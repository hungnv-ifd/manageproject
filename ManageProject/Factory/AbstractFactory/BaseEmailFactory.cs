﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageProject.Factory.AbstractFactory
{
    abstract class BaseEmailFactory
    {
         public abstract BaseEmailBody GetBody();
    }
}
