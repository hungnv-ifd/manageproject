﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManageProject.Factory.AbstractFactory
{
    class DatabaseFactory : BaseEmailFactory
    {
        public override BaseEmailBody GetBody()
        {
            return new BodyDatabase();
        }
    }
    class ProtocolFactory : BaseEmailFactory
    {
        public override BaseEmailBody GetBody()
        {
            return new BodyProtocol();
        }
    }
    class ServerFactory : BaseEmailFactory
    {
        public override BaseEmailBody GetBody()
        {
            return new BodyServer();
        }
    }
}