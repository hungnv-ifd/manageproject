﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;

namespace ManageProject.Controllers
{
    public class EnvironmentsController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: Environments
        public ActionResult Index()
        {
            var enviroments = unitOfWork.Repository<Enviroment>().Get().Where(x => x.IsDelete == false);
            return View(enviroments.ToList());
        }

        // GET: Environments/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Enviroment enviroment = unitOfWork.Repository<Enviroment>().GetbyID(id);
            if (enviroment == null)
            {
                return HttpNotFound();
            }
            return View(enviroment);
        }

        // GET: Environments/Create
        public ActionResult Create(string IdUser)
        {
            if (User.IsInRole("Admin"))
            {
                ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(x => x.IsDelete == false), "ProjectId", "ProjectName");
                ViewBag.IdServerEnvironment = new SelectList(unitOfWork.Repository<ServerEnvironments>().Get().Where(x => x.IsDelete == false), "IdServer", "ServerName");
            }
            else
            {
                var tmp = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject").Select(s => s.managerProject).Distinct();
                ViewBag.ProjectId = new SelectList(tmp.ToList(), "ProjectId", "ProjectName");
            }
            return View();
        }

        // POST: Environments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Environment,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Enviroment enviroment)
        {
            if (ModelState.IsValid)
            {
                enviroment.Createby = User.Identity.Name;
                enviroment.Createdate = Convert.ToDateTime(DateTime.Now);
                enviroment.Environment = Guid.NewGuid().ToString();
                unitOfWork.Repository<Enviroment>().Insert(enviroment);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error", "error");
            ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get(), "ProjectId", "ProjectName", enviroment.ProjectEnvironments.Select(e => e.Project));
            return View(enviroment);
        }

        // GET: Environments/Edit/5
        public ActionResult Edit(string id, string IdUser, string ProjectId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Enviroment Enviroment = unitOfWork.Repository<Enviroment>().GetbyID(id);
                if (Enviroment == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(x => x.IsDelete == false), "ProjectId", "ProjectName", Enviroment.ProjectEnvironments.Select(e => e.Project));

                return View(Enviroment);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Environments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Environment,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Enviroment enviroment)
        {
            if (ModelState.IsValid)
            {
                enviroment.Updateby = User.Identity.Name;
                enviroment.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Enviroment>().Update(enviroment);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");

            ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get(), "ProjectId", "ProjectName", enviroment.ProjectEnvironments.Select(e => e.Project));

            return View(enviroment);
        }

        // GET: Environments/Delete/5
        public ActionResult Delete(string id, string IdUser, string ProjectId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Enviroment Enviroment = unitOfWork.Repository<Enviroment>().GetbyID(id);
                if (Enviroment == null)
                {
                    return HttpNotFound();
                }
                return View(Enviroment);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Environments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Enviroment Enviroment = unitOfWork.Repository<Enviroment>().GetbyID(id);
            Enviroment.Deleteby = User.Identity.Name;
            Enviroment.Deletedate = Convert.ToDateTime(DateTime.Now);
            Enviroment.IsDelete = true;
            SetAlert("Update Successfully", "success");
            unitOfWork.Save();
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult DeleteEnviroment(string id)
        {
            bool status = false;
            try
            {
                Enviroment Enviroment = unitOfWork.Repository<Enviroment>().GetbyID(id);
                Enviroment.Deleteby = User.Identity.Name;
                Enviroment.Deletedate = Convert.ToDateTime(DateTime.Now);
                Enviroment.IsDelete = true;
                unitOfWork.Save();
                status = true;
                SetAlert("Update Successfully", "success");

            }
            catch (Exception)
            {
                SetAlert("Update Error", "error");
            }

            return Json(status);
        }
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
