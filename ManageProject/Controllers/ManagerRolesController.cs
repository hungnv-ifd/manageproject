﻿using log4net;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ManageProject.Controllers
{
   // [Authorize(Roles ="Admin")]
    public class ManagerRolesController : Controller
    {
        private static readonly ILog logger = LogManager.GetLogger(

         System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static ApplicationDbContext db = new ApplicationDbContext();
        public static GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: ManagerRoles
        public ActionResult Index()
        {
            var model = db.Roles.ToList();
            return View(model);
        }

        // GET: ManagerRoles/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ManagerRoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ManagerRoles/Create
        [HttpPost]
        public ActionResult Create(IdentityRole role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Roles.Add(role);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var tmp = db.Roles.Where(x => x.Name == role.Name);
                if (tmp != null)
                {
                    ModelState.AddModelError("", "Role này đã tồn tại!");
                }
                else
                {
                    ModelState.AddModelError("", ex.Message);
                }

                return View(role);
            }
        }

        // GET: ManagerRoles/Edit/5
        public ActionResult Edit(string id)
        {
            var model = db.Roles.Find(id);
            return View(model);
        }

        // POST: ManagerRoles/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, IdentityRole role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var model = db.Roles.Find(id);
                    if (model != null)
                    {
                        model.Name = role.Name;
                        db.SaveChanges();
                    }

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var tmp = db.Roles.Where(x => x.Name == role.Name);
                if (tmp != null)
                {
                    logger.Error(ex.Message);
                    ModelState.AddModelError("", "Roles này đã tồn tại!");
                }
                else
                {
                    logger.Error(ex.Message);
                    ModelState.AddModelError("", ex.Message);
                }

                return View();
            }
        }

        // GET: ManagerRoles/Delete/5
        public ActionResult Delete(string id)
        {
            var model = db.Roles.Find(id);
            return View(model);
        }
        /// <summary>
        /// delete role 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // POST: ManagerRoles/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteRole(string id)
        {
            IdentityRole model = null;
            try
            {
                model = db.Roles.Find(id);
                if (ModelState.IsValid)
                {
                    if (model != null)
                    {
                        db.Roles.Remove(model);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Idproject"></param>
        /// <returns></returns>
        public static bool IsDenied(string id, string Idproject)
        {
            var user = unitOfWork.Repository<UserProject>().Get().Where(x => x.UserId == id && x.Project == Idproject).SingleOrDefault();

            if (user != null)
            {
                return true;
            }
            return false;
        }
        public static bool IsDenied(string id)
        {
            var user = unitOfWork.Repository<UserProject>().Get().Where(x => x.UserId == id).FirstOrDefault();

            if (user != null)
            {
                return true;
            }
            return false;
        }
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}