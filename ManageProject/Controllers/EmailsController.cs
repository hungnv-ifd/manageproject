﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ManageProject.BuilderDesignPattern;
using ManageProject.DAL.NUoW;
using ManageProject.Factory.AbstractFactory;
using ManageProject.Models;
using Microsoft.AspNet.Identity;

namespace ManageProject.Controllers
{
    public class EmailsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        private BaseEmailFactory emailFactory;
        enum TypeMail { Database, Protocol, Server };

        // GET: Emails
        public ActionResult Index()
        {
            ViewBag.Project = unitOfWork.Repository<ManageProjects>().Get();

            List<string> tmp = new List<string>();
            tmp.Add("Database");
            tmp.Add("Protocol");
            tmp.Add("Server");
            ViewBag.ListUser = db.Users.Select(x => x.Email).ToList();
            ViewBag.Type = tmp.ToList();
            return View();
        }

        // GET: Emails/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Email email = unitOfWork.Repository<Email>().GetbyID(id);
            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        // GET: Emails/Create
        public ActionResult Create()
        {
            ViewBag.listTypeMail = Enum.GetValues(typeof(TypeMail));
            return View();
        }

        // POST: Emails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdEmail,Name,Type,Description,Subject,Body")] Email email, string Type)
        {
            
            if (ModelState.IsValid)
            {
                EmailDirector director = new EmailDirector();
                ViewBag.listTypeMail = Enum.GetValues(typeof(TypeMail));
                var tmp = unitOfWork.Repository<Email>().Get().Where(p => p.Type == Type).SingleOrDefault();
                if (tmp != null)
                {
                    
                    if (email.Body == null)
                    {
                        switch (Type)
                        {
                            case "Protocol":
                                {
                                    emailFactory = new ProtocolFactory();
                                    tmp.Description = email.Description;
                                    tmp.Subject = email.Subject;
                                    tmp.Body = emailFactory.GetBody().Body();
                                    unitOfWork.Repository<Email>().Update(tmp);
                                }; break;
                            case "Database":
                                {
                                    emailFactory = new DatabaseFactory();
                                    tmp.Description = email.Description;
                                    tmp.Subject = email.Subject;
                                    tmp.Body = emailFactory.GetBody().Body();
                                    unitOfWork.Repository<Email>().Update(tmp);
                                }; break;
                            case "Server":
                                {
                                    emailFactory = new ServerFactory();
                                    tmp.Description = email.Description;
                                    tmp.Subject = email.Subject;
                                    tmp.Body = emailFactory.GetBody().Body();
                                    unitOfWork.Repository<Email>().Update(tmp);
                                }; break;
                        }
                    }
                    else
                    {
                        tmp.Description = email.Description;
                        tmp.Subject = email.Subject;
                        tmp.Body = email.Body;
                        unitOfWork.Repository<Email>().Update(tmp);
                    }
                    unitOfWork.Save();
                    SetAlert("Change existing Template in Database Successfully", "success");
                    return RedirectToAction("Index");
                    
                }
                if(email.Body == null)
                {
                    switch (Type)
                    {
                        case "Protocol": {
                                EmailBuilder e = new ForgotPassword();
                                director.setEmailBuilder(e);
                                emailFactory = new ProtocolFactory();
                                director.constructorEmailDirection(email.Type, email.Description, email.Subject, email.Body = emailFactory.GetBody().Body());
                                unitOfWork.Repository<Email>().Insert(e.getEmail());
                            }; break;
                        case "Database": {
                                EmailBuilder e = new SendPassword();
                                director.setEmailBuilder(e);
                                emailFactory = new DatabaseFactory();
                                director.constructorEmailDirection(email.Type, email.Description, email.Subject, email.Body = emailFactory.GetBody().Body());
                                unitOfWork.Repository<Email>().Insert(e.getEmail());
                            }; break;
                        case "Server": {
                                EmailBuilder e = new SendPassword();
                                director.setEmailBuilder(e);
                                emailFactory = new ServerFactory();
                                director.constructorEmailDirection(email.Type, email.Description, email.Subject, email.Body = emailFactory.GetBody().Body());
                                unitOfWork.Repository<Email>().Insert(e.getEmail());
                            };break;
                    }
                }
                else
                {
                    EmailBuilder e = new SendPassword();
                    director.setEmailBuilder(e);
                    director.constructorEmailDirection(email.Type, email.Description, email.Subject, email.Body);
                    unitOfWork.Repository<Email>().Insert(e.getEmail());
                }
                
                unitOfWork.Save();
                SetAlert("Create Template Successfully", "success");
                return RedirectToAction("Index");
            }
            return View(email);
        }

        // GET: Emails/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Email email = unitOfWork.Repository<Email>().GetbyID(id);
            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        // POST: Emails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdEmail,Type,Description,Subject,Body")] Email email)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Email>().Update(email);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(email);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        

        public string ChangeHtml(string name, string type, string namepro, string namedb, string namesv,string nameenv)
        {
            var t = "";
            try
            {
                if (type == "Database")
                {
                    emailFactory = new DatabaseFactory();
                    t = emailFactory.GetBody().ShowBody(name, namedb);
                }
                else if (type == "Protocol")
                {
                    emailFactory = new ProtocolFactory();
                    t = emailFactory.GetBody().ShowBody(name, namepro);
                }
                else
                {
                    emailFactory = new ServerFactory();
                    t = emailFactory.GetBody().ShowBody(name, namesv, nameenv);
                }
            }
            catch { }
            return t;
        }

        public JsonResult ChangeProtocol(string name)
        {
            var x = unitOfWork.Repository<ProjectEnvironments>().Get(filter: p => p.IsDelete == false && p.Project == name && p.Protocols.IsDelete == false && p.IsDelete == false, includeProperties: "Protocols").Select(s=>s.Protocols.HostName).Distinct().ToList();
            return Json(new { ListProtocols = x }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeDatabase(string name)
        {
            var x = unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.IsDelete == false && s.Project == name && s.Databases.IsDelete == false && s.IsDelete == false, includeProperties: "Databases").Select(s=>s.Databases.DatabaseName).Distinct().ToList();
            return Json(new { ListDatabase = x }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeServer(string name)
        {
            var x = from se in unitOfWork.Repository<ServerEnvironments>().Get(filter: s => s.IsDelete == false && s.Servers.IsDelete == false)
                    from e in unitOfWork.Repository<Enviroment>().Get(filter: s => s.IsDelete == false)
                    from p in unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.IsDelete == false)
                    where se.Environment == e.Environment && e.Environment == p.Environment && p.Project == name
                    group se by new
                    {
                        se.Servers.ServerName,
                        se.Environments.Type,
                    } into r
                    select new
                    {
                        r.Key.ServerName,
                        r.Key.Type
                    };
            var y = x.Select(s => s.ServerName);
            var z = x.Select(s => s.Type);
            return Json(new { ListServer = y, ListEnvironment = z}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SendMail(string body, string toEmail, string Bcc, string Cc,string toaddress)
        {
            bool isSend = false;
            try
            {

                body = body.Replace("{FullName}", toEmail);
                
                var mailUser = toEmail;
                var username = ConfigurationManager.AppSettings["userName"].ToString();
                var password = ConfigurationManager.AppSettings["userPassword"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
                var host = ConfigurationManager.AppSettings["Host"].ToString();
                var client = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(username, password),
                    EnableSsl = true,
                };
                var from = new MailAddress(username, "AdminTest");
                var to = new MailAddress(mailUser);
                var mail = new MailMessage(from, to)
                {
                    Subject = "Send password",
                    Body = body,
                    IsBodyHtml = true,
                };


                if (Bcc != "")
                {
                    string[] splitBcc;
                    splitBcc = Bcc.Split('-');
                    for (int i = 0; i < splitBcc.Length - 1; i++)
                    {
                        mail.Bcc.Add(splitBcc[i]);
                    }
                }
                if (Cc != "")
                {
                    string[] splitCc;
                    splitCc = Cc.Split('-');
                    for (int i = 0; i < splitCc.Length - 1; i++)
                    {
                        mail.Bcc.Add(splitCc[i]);
                    }
                }

                try
                {
                    client.Send(mail);
                    isSend = true;
                    SetAlert("Send Email Successfully", "success");
                }
                catch (Exception)
                {
                    SetAlert("Send Email Error", "error");
                    
                }
                
                
                return Json(new { isSend = isSend },JsonRequestBehavior.AllowGet);
            }
          
            catch (Exception ex)
            {
                SetAlert("Send Email Error", "error");
            }
           
            return Json(new { isSend = isSend }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Set Alert for Status update data
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
        public ActionResult listTempalteEmail()
        {
            return View(unitOfWork.Repository<Email>().Get());
        }
        [HttpPost]
        public JsonResult DeleteEmail(string Id)
        {
            bool status = false;
            var model = unitOfWork.Repository<Email>().Get().Where(m => m.IdEmail == Id).SingleOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.Repository<Email>().Delete(model);
                    unitOfWork.Save();
                    SetAlert("Update success", "success");
                    status = true;
                    return Json(status);
                }
            }
            catch (Exception ex)
            {
                
                SetAlert("Update Error", "error");
                ModelState.AddModelError("", ex.Message);
            }
            return Json(status);
        }
    }
}
