﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;

namespace ManageProject.Controllers
{
    public class ProjectEnvironmentsController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: ProjectEnvironments
        public ActionResult Index()
        {
            var projectEnvironments = unitOfWork.Repository<ProjectEnvironments>().Get().Where(p => p.IsDelete == false && p.Projects.IsDelete == false && p.Environments.IsDelete == false);
            
            return View(projectEnvironments.ToList());
        }

        // GET: ProjectEnvironments/Details/5
        public ActionResult Details(string ProjectEnvironmentId,string EnvironmentId, string ProjectId)
        {
            if (ProjectEnvironmentId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectEnvironments projectEnvironments = unitOfWork.Repository<ProjectEnvironments>().GetbyID(ProjectEnvironmentId);
            if (projectEnvironments == null)
            {
                return HttpNotFound();
            }
            return View(projectEnvironments);
        }

        // GET: ProjectEnvironments/Create
        public ActionResult Create(string IdUser,string ProjectId)
        {

            if (User.IsInRole("Admin"))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type");
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", ProjectId);
            }
            
            return View();
        }
        
        // POST: ProjectEnvironments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.Environment == projectEnvironments.Environment).ToList();
            if (isTask.Count > 0)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            var t = isTask.SingleOrDefault();
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if(t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if(t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            
            SetAlert("Update Error: Value is already exist or value is error !", "error");

            ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "IdEnviroment", "Type", projectEnvironments.Environment);
            ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "ProjectId", "ProjectName", projectEnvironments.Project);
            return View(projectEnvironments);
        }
        public ActionResult AddDatabase(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin"))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdDatabase = new SelectList(unitOfWork.Repository<Databases>().Get().Where(d => d.IsDelete == false), "Database", "DatabaseName");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDatabase([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdDatabase == projectEnvironments.IdDatabase).ToList();
            if(isTask.Count > 0)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            var t = isTask.SingleOrDefault();
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }
        public ActionResult AddProtocol(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin"))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdProtocol = new SelectList(unitOfWork.Repository<Protocols>().Get().Where(d => d.IsDelete == false), "Protocol", "HostName");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProtocol([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdProtocol == projectEnvironments.IdProtocol).ToList();
            if (isTask.Count > 0)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            var t = isTask.SingleOrDefault();
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }

        public ActionResult AddWebsite(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin"))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdWebsite = new SelectList(unitOfWork.Repository<Websites>().Get().Where(d => d.IsDelete == false), "Website", "Name");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddWebsite([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdWebsite == projectEnvironments.IdWebsite).ToList();
            if (isTask.Count > 0)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            var t = isTask.SingleOrDefault();
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }
        public ActionResult AddAPI(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin"))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdAPI = new SelectList(unitOfWork.Repository<APIs>().Get().Where(d => d.IsDelete == false), "API", "InfoAPI");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAPI([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
            {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdAPI == projectEnvironments.IdAPI).ToList();
            if (isTask.Count > 0)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            var t = isTask.SingleOrDefault();
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }
        // GET: ProjectEnvironments/Edit/5
        public ActionResult Edit(string IdUser, string ProjectEnvironmentId, string EnvironmentId,string ProjectId)
        {
            if(User.IsInRole("Admin"))
            {
                if (ProjectEnvironmentId==null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ProjectEnvironments projectEnvironments = unitOfWork.Repository<ProjectEnvironments>().GetbyID(ProjectEnvironmentId);

                if (projectEnvironments == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", projectEnvironments.Environment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", projectEnvironments.Project);
                return View(projectEnvironments);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: ProjectEnvironments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Project,Environment,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.Environment == projectEnvironments.Environment).SingleOrDefault();
            if (isTask != null && isTask.IsDelete == false)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (isTask != null && isTask.IsDelete == true)
            {
                projectEnvironments.IsDelete = false;
                projectEnvironments.Updateby = User.Identity.Name;
                projectEnvironments.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Update(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if(isTask == null && ModelState.IsValid)
            {
                projectEnvironments.Updateby = User.Identity.Name;
                projectEnvironments.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Update(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");
            ViewBag.EnvironmentId = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "IdEnviroment", "Type", projectEnvironments.Environment);
            ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "ProjectId", "ProjectName", projectEnvironments.Project);
            return View(projectEnvironments);
        }

        // GET: ProjectEnvironments/Delete/5
        public ActionResult Delete(string IdUser, string id, string ProjectId,string EnvironmentId)
        {
            if(User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, id))
            {
                ProjectEnvironments projectEnvironments;
                if (ProjectId != null && EnvironmentId != null)
                {
                    var projectEnv = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == ProjectId && x.Environment == EnvironmentId);
                    
                    id = String.Join(",", unitOfWork.Repository<ProjectEnvironments>().Get().Where(x=>x.Project == ProjectId && x.Environment == EnvironmentId).Select(x=>x.Id));
                    
                }
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                projectEnvironments = unitOfWork.Repository<ProjectEnvironments>().GetbyID(id);
                if (projectEnvironments == null)
                {
                    return HttpNotFound();
                }
                return View(projectEnvironments);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        // POST: ProjectEnvironments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string IdUser, string id, string ProjectId, string EnvironmentId)
        {
            ProjectEnvironments projectEnvironments;
            if (ProjectId != null || EnvironmentId != null)
            {
                id = String.Join(",", unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == ProjectId && x.Environment == EnvironmentId).Select(x => x.Id));
            }
            projectEnvironments = unitOfWork.Repository<ProjectEnvironments>().GetbyID(id);
            projectEnvironments.IsDelete = true;
            projectEnvironments.Deleteby = User.Identity.Name;
            projectEnvironments.Deletedate = Convert.ToDateTime(DateTime.Now);
            unitOfWork.Save();
            SetAlert("Update Successfully", "success");
            
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        private void SetAlert(string message, string type)
        {
            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }
        }
    }
}
