﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using Microsoft.AspNet.Identity;

namespace ManageProject.Controllers
{
    public class WebsitesController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: Websites
        public ActionResult Index()
        {
            var websites = unitOfWork.Repository<Websites>().Get().Where(x => x.IsDelete == false);
            return View(websites.ToList());
        }

        // GET: Websites/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
            if (websites == null)
            {
                return HttpNotFound();
            }
            return View(websites);
        }

        // GET: Websites/Create
        public ActionResult Create(string IdUser,string IdEnvironment)
        {
            if (User.IsInRole("Admin"))
            {
                if(IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ViewBag.tmp = y.ToList();
            }

            return View();

        }

        // POST: Websites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Website,Name,InfoWebsite")] Websites websites, string IdEnvironment, string IdProject)
        {
            if (ModelState.IsValid)
            {
                websites.Createdate = Convert.ToDateTime(DateTime.Now);
                websites.Createby = User.Identity.Name.ToString();
                websites.Website = Guid.NewGuid().ToString();
                unitOfWork.Repository<Websites>().Insert(websites);
                
                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdWebsite = websites.Website,
                        Createdate = websites.Createdate,
                        Createby = websites.Createby
                    };
                    websites.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, websites.ProjectEnvId*/);
            return View(websites);
        }

        // GET: Websites/Edit/5
        public ActionResult Edit(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
                if (websites == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                return View(websites);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Websites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Website,Name,InfoWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Websites websites)
        {
            if (ModelState.IsValid)
            {
                websites.Updateby = User.Identity.GetUserName().ToString();
                websites.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Websites>().Update(websites);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, websites.ProjectEnvId*/);
            return View(websites);
        }

        // GET: Websites/Delete/5
        public ActionResult Delete(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
                if (websites == null)
                {
                    return HttpNotFound();
                }
                return View(websites);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Websites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
            websites.Deleteby = User.Identity.GetUserName().ToString();
            websites.Deletedate = Convert.ToDateTime(DateTime.Now);
            websites.IsDelete = true;
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdWebsite == id).SingleOrDefault();
            if (isTask != null && isTask.IsDelete == false)
            {
                isTask.IsDelete = true;
                isTask.Deleteby = websites.Deleteby;
                isTask.Deletedate = websites.Deletedate;
                unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
            }
            unitOfWork.Save();
            SetAlert("Update Successfully", "success");

            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        /// Ajax delete Website
        /// 
        public JsonResult DeleteWebsite(string id)
        {
            bool status = false;
            try
            {
                Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
                websites.Deleteby = User.Identity.GetUserName().ToString();
                websites.Deletedate = Convert.ToDateTime(DateTime.Now);
                websites.IsDelete = true;
                var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdWebsite == id).SingleOrDefault();
                if (isTask != null && isTask.IsDelete == false)
                {
                    isTask.IsDelete = true;
                    isTask.Deleteby = websites.Deleteby;
                    isTask.Deletedate = websites.Deletedate;
                    unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                status = true;
            }
            catch (Exception)
            {
                SetAlert("Update Error", "error");
            }

            return Json(status);
        }
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
