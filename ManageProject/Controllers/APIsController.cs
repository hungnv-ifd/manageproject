﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using Microsoft.AspNet.Identity;
namespace ManageProject.Controllers
{
    public class APIsController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: APIs
        public ActionResult Index()
        {
            var aPIs = unitOfWork.Repository<APIs>().Get().Where(a => a.IsDelete == false);
            return View(aPIs.ToList());
        }

        // GET: APIs/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
            if (aPIs == null)
            {
                return HttpNotFound();
            }
            return View(aPIs);
        }

        // GET: APIs/Create
        public ActionResult Create(string IdUser,string IdEnvironment)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
            items.Add(new SelectListItem() { Text = "External ", Value = "External" });

            ViewBag.Type = items.ToList();
            if (User.IsInRole("Admin"))
            {
                if(IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment);
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false);
                }
            }
            else
            {
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ViewBag.tmp = y.ToList();
            }
                return View();
        }

        // POST: APIs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "API,InfoAPI,UserName,Password,Type")] APIs aPIs,string IdEnvironment,string IdProject)
        {

            if (ModelState.IsValid)
            {
                aPIs.Createby = User.Identity.Name;
                aPIs.Createdate = Convert.ToDateTime(DateTime.Now);
                aPIs.API = Guid.NewGuid().ToString();
                unitOfWork.Repository<APIs>().Insert(aPIs);
               
                if(IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdAPI = aPIs.API,
                        Createdate = aPIs.Createdate,
                        Createby = aPIs.Createby
                    };
                    aPIs.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                return Redirect(Request.UrlReferrer.ToString());
            }

            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
            items.Add(new SelectListItem() { Text = "External ", Value = "External" });

            ViewBag.Type = items.ToList();
            return View(aPIs);
        }

        // GET: APIs/Edit/5
        public ActionResult Edit(string id,string IdUser,string EnvironmentId)
        {

            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
                if (aPIs == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false);
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
                items.Add(new SelectListItem() { Text = "External ", Value = "External" });
                ViewBag.Type = items.ToList();
                return View(aPIs);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: APIs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "API,InfoAPI,UserName,Password,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] APIs aPIs)
        {
            if (ModelState.IsValid)
            {
                
                aPIs.Updateby = User.Identity.Name;
                aPIs.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<APIs>().Update(aPIs);
                unitOfWork.Save();
                return Redirect(Request.UrlReferrer.ToString());
            }
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<ProjectEnvironments>().Get(), "IdEnviroment", "ProjectId");
            return View(aPIs);
        }

        // GET: APIs/Delete/5
        public ActionResult Delete(string id ,string IdUser, string ProjectEnvironmentId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
                if (aPIs == null)
                {
                    return HttpNotFound();
                }
                return View(aPIs);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: APIs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
            aPIs.Deleteby = User.Identity.Name;
            aPIs.Deletedate = Convert.ToDateTime(DateTime.Now);
            aPIs.IsDelete = true;

            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdAPI == id).SingleOrDefault();
            if (isTask != null && isTask.IsDelete == false)
            {
                isTask.IsDelete = true;
                isTask.Deleteby = aPIs.Deleteby;
                isTask.Deletedate = aPIs.Deletedate;
                unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
            }
            unitOfWork.Save();
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        ///Ajax Delete API
        ///
        public JsonResult DeleteAPI(string id, string IdUser, string ProjectId, string EnvironmentId)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.Project == ProjectId && s.Environment == EnvironmentId && s.IdAPI == id).SingleOrDefault();

            bool status = false;
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, EnvironmentId))
            {
                if (id == null)
                {
                    return Json(status);
                }
                APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
                if (aPIs == null)
                {
                    return Json(status);
                }
                else
                {
                    aPIs.Deleteby = User.Identity.Name;
                    aPIs.Deletedate = Convert.ToDateTime(DateTime.Now);
                    aPIs.IsDelete = true;
                    if(isTask != null && isTask.IsDelete == false)
                    {
                        isTask.IsDelete = true;
                        isTask.Deleteby = aPIs.Deleteby;
                        isTask.Deletedate = aPIs.Deletedate;
                        unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                    }
                    unitOfWork.Save();
                    status = true;
                    return Json(status);
                }
               
            }
            return Json(status);
        }
    }
}
