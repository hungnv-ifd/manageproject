﻿using ManageProject.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;

namespace ManageProject.Controllers
{
    public class DatabasesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Databases
        public ActionResult Index()
        {
            var databases = unitOfWork.Repository<Databases>().Get().Where(x => x.IsDelete == false);
            return View(databases.ToList());
        }

        // GET: Databases/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
            if (databases == null)
            {
                return HttpNotFound();
            }
            return View(databases);
        }

        // GET: Databases/Create
        public ActionResult Create(string IdUser,string IdEnvironment)
        {
            if (User.IsInRole("Admin"))
            {
                if(IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ApplicationUser model = db.Users.Find(IdUser);
                ViewBag.tmp = y.ToList();
            }
            return View();
        }

        // POST: Databases/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Database,InstanceName,DatabaseName,UserName,Password")] Databases databases, string IdEnvironment, string IdProject)
        {
            if (ModelState.IsValid)
            {
                databases.Createby = User.Identity.Name;
                databases.Createdate = Convert.ToDateTime(DateTime.Now);
                databases.Database = Guid.NewGuid().ToString();
                unitOfWork.Repository<Databases>().Insert(databases);
                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdDatabase = databases.Database,
                        Createdate = databases.Createdate,
                        Createby = databases.Createby
                    };
                    databases.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId");

            return View(databases);
        }

        // GET: Databases/Edit/5
        public ActionResult Edit(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
                if (databases == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();

                return View(databases);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        // POST: Databases/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Database,InstanceName,DatabaseName,UserName,Password,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Databases databases)
        {
            if (ModelState.IsValid)
            {
                databases.Updateby = User.Identity.Name;
                databases.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Databases>().Update(databases);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId");
            SetAlert("Update Error", "error");

            return View(databases);
        }

        // GET: Databases/Delete/5
        public ActionResult Delete(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
                if (databases == null)
                {
                    return HttpNotFound();
                }
                return View(databases);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        // POST: Databases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdDatabase == id).SingleOrDefault();
            Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
            databases.Deleteby = User.Identity.Name;
            databases.Deletedate = Convert.ToDateTime(DateTime.Now);
            databases.IsDelete = true;
            SetAlert("Update Successfully", "success");
            if (isTask != null && isTask.IsDelete == false)
            {
                isTask.IsDelete = true;
                isTask.Deleteby = databases.Deleteby;
                isTask.Deletedate = databases.Deletedate;
                unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
            }
            unitOfWork.Save();
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        public async Task<ActionResult> sendMailPassDatabase(string id, string IdUser, string ProjectId)
        {
            var model = db.Users.Where(x => x.Id == IdUser).FirstOrDefault();
            var userMail = await UserManager.FindByNameAsync(model.Email);
            var contentPass = unitOfWork.Repository<Databases>().Get().Where(x => x.Database == id).Select(x => x.Password).FirstOrDefault();

            var contentDb = unitOfWork.Repository<Databases>().Get().Where(x => x.Database == id).Select(x => x.DatabaseName).FirstOrDefault();

            var contentProject = unitOfWork.Repository<ManageProjects>().Get().Where(x => x.Project == ProjectId).Select(x => x.ProjectName).FirstOrDefault();

            await UserManager.SendEmailAsync(userMail.Id, "Password Database", "Password database of project: " + contentProject + " ,database: " + contentDb + " is:" + contentPass);

            return View();
        }

        ///Ajax delete Database
        ///
        public JsonResult DeleteDatabase(string id)
        {
            bool status = false;
            try
            {
                Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
                databases.Deleteby = User.Identity.Name;
                databases.Deletedate = Convert.ToDateTime(DateTime.Now);
                databases.IsDelete = true;
                var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdDatabase == id).SingleOrDefault();

                if (isTask != null && isTask.IsDelete == false)
                {
                    isTask.IsDelete = true;
                    isTask.Deleteby = databases.Deleteby;
                    isTask.Deletedate = databases.Deletedate;
                    unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                }
                unitOfWork.Save();
                status = true;
            }
            catch (Exception)
            {

            }

            return Json(status);
        }
        [HttpPost]
        public JsonResult SendMail(string id, string IdUser, string ProjectId)
        {
            bool isSend = false;
            try
            {
                
                var mailUser = User.Identity.GetUserName();
                var NameUser = db.Users.Where(x => x.Id == IdUser).Select(x => x.FullName).FirstOrDefault().ToString();
                var username = ConfigurationManager.AppSettings["userName"].ToString();
                var password = ConfigurationManager.AppSettings["userPassword"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
                var host = ConfigurationManager.AppSettings["Host"].ToString();
                var client = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(username, password),
                    EnableSsl = true,
                };
                var from = new MailAddress(username, "AdminTest");
                var to = new MailAddress(mailUser);
                var mail = new MailMessage(from, to)
                {
                    Subject = "Send database's password",
                    Body = createBody(id, NameUser),
                    IsBodyHtml = true,
                };
                client.Send(mail);
                isSend = true;
                return Json(new { isSend = isSend });
            }
            catch (Exception ex)
            {
               
            }

            return Json(new { isSend = isSend });
        }
        public string createBody(string idDb, string nameUser)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Views/TemplateEmail/MailDatabase.cshtml")))

            {

                body = reader.ReadToEnd();

            }
            var tmp = unitOfWork.Repository<Databases>().Get().Where(x => x.Database == idDb).FirstOrDefault();

            body = body.Replace("{FullName}", nameUser);
            body = body.Replace("{InstanceName}", tmp.InstanceName); //replacing the required things  

            body = body.Replace("{DatabaseName}", tmp.DatabaseName);

            body = body.Replace("{UserName}", tmp.UserName);
            body = body.Replace("{Password}", tmp.Password);
            body = body.Replace("{CreateDate}", tmp.Createdate.ToString());
            body = body.Replace("{UpdateDate}", tmp.Updatedate.ToString());
            
            return body;

        }
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
