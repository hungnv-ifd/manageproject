﻿using ManageProject1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManageProject1.Controllers
{
    public class ManageProjectsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: ManageProjects
        public ActionResult Index()
        {
            var model = db.ManageProjects.ToList();
            return View(model);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ManageProject project)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    db.ManageProjects.Add(project);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View(project);
        }
        public ActionResult Edit(string id)
        {
            var model = db.ManageProjects.Find(id);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ManageProject project)
        {
            var model = db.ManageProjects.Find(project.ProjectId);
            try
            {
                if(ModelState.IsValid)
                {
                    model = project;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View(model);

        }
        public ActionResult Delete(string id)
        {
            var model = db.ManageProjects.Find(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult ComfirmDelete(string id)
        {
            var model = db.ManageProjects.Find(id);
            try
            {
                if (ModelState.IsValid)
                {
                    db.ManageProjects.Remove(model);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return RedirectToAction("Index");
            
        }
    }
}