﻿using log4net;
using ManageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ManageProject.DAL;
using System.Net;
using ManageProject.DAL.NUoW;

namespace ManageProject.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
       
        public static string[] ArrayStage = new string[] { "Complete", "Develop", "Perform" }; //set value for Stage
        private static readonly ILog logger = LogManager.GetLogger(
        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); //log 4net

        ApplicationDbContext db = new ApplicationDbContext();
        GenericUnitOfWork unitOfWork = new GenericUnitOfWork();

        // GET: ManageIProject
        public ActionResult Index(string projectName)
        {
            var model = unitOfWork.Repository<ManageProjects>().Get().Where(x => x.IsDelete == false).ToList();
            ViewBag.listProject = model;
            var tmp = ArrayStage.ToList();
            ViewBag.Stage = tmp;
            if (!String.IsNullOrEmpty(projectName))
            {
                model = unitOfWork.Repository<ManageProjects>().Get().Where(x => x.ProjectName.Contains(projectName) && x.IsDelete == false).ToList();
            }
            return View(model);
        }

        /// <summary>
        /// Create Project 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult Create(string IdUser)
        {

            if (User.IsInRole("Admin")|| ManagerRolesController.IsDenied(IdUser))
            {
                ViewBag.statusJoinProject = new SelectList(ArrayStage.ToList());
                return View();
            }
            else
            {
                SetAlert("Bạn không có quyền truy cập", "error");
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Create Project - method post
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Project,ProjectName,Summary,LanguageCode,Tech,removeServer,isJoin,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete,DateDeployment,InfoFlyway")] ManageProjects manageProjects)
        {
            try
            {
                ViewBag.statusJoinProject = new SelectList(ArrayStage.ToList());
                if (ModelState.IsValid)
                {
                    manageProjects.Createby = User.Identity.Name;
                    manageProjects.Createdate = Convert.ToDateTime(DateTime.Now);
                    manageProjects.Project = Guid.NewGuid().ToString();
                    unitOfWork.Repository<ManageProjects>().Insert(manageProjects);
                    unitOfWork.Save();
                    return Redirect(Request.UrlReferrer.ToString());


                }
                else
                {
                    logger.Info("Create project fail!!!!");
                   
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            //return Json(status = false);
            return View(manageProjects);
        }
        /// <summary>
        /// Edit Project 
        /// </summary>
        /// <param name="ProjectId"></param>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(string id,string IdUser)
        {
            var model = unitOfWork.Repository<ManageProjects>().GetbyID(id);

            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, id))
            {
                ViewBag.DateDeployment = model.DateDeployment;
                ViewBag.statusJoinProject = new SelectList(ArrayStage.ToList());
                return View(model);
            }
            else
            {
                SetAlert("Bạn không có quyền", "error");
                return RedirectToAction("Index");
            }
        }
        /// <summary>
        /// Edit Project  -Method POST
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Project,ProjectName,Summary,LanguageCode,Tech,removeServer,isJoin,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete,DateDeployment,InfoFlyway")] ManageProjects manageProjects)
        {
            var model = unitOfWork.Repository<ManageProjects>().Get().Where(m => m.Project == manageProjects.Project).SingleOrDefault();
            try
            {
                if (model!=null && ModelState.IsValid)
                {
                    model.Updateby = User.Identity.Name;
                    model.Updatedate = Convert.ToDateTime(DateTime.Now);
                    model.ProjectName = manageProjects.ProjectName;
                    model.Summary = manageProjects.Summary;
                    model.LanguageCode = manageProjects.LanguageCode;
                    model.Tech = manageProjects.Tech;
                    model.removeServer = manageProjects.removeServer;
                    model.isJoin = manageProjects.isJoin;
                    model.DateDeployment = manageProjects.DateDeployment;
                    model.InfoFlyway = manageProjects.InfoFlyway;

                    ViewBag.statusJoinProject = new SelectList(ArrayStage.ToList());
                    unitOfWork.Save();
                    SetAlert("Update success", "success");
                    return Redirect(Request.UrlReferrer.ToString());
                }
                 else
                {
                    logger.Info("Edit unsuccessful!!!");
                }
                
            }
            catch (Exception ex)
            {
                SetAlert("Update Error", "error");
                ModelState.AddModelError("", ex.Message);
            }
            return View(model);

        }
        /// <summary>
        /// Delete Project 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult Delete(string id,string IdUser)
        {
            var model = unitOfWork.Repository<ManageProjects>().GetbyID(id);
            if (User.IsInRole("Admin")||ManagerRolesController.IsDenied(IdUser,id))
            {
                return View(model);
            }
            else
            {
                SetAlert("Bạn không có quyền truy cập!", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        /// <summary>
        /// Delete Project - Method Post
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult ComfirmDelete(string id)
        {
            var model = unitOfWork.Repository<ManageProjects>().GetbyID(id);
            try
            {
                if (ModelState.IsValid)
                {
                    model.Deleteby = User.Identity.Name;
                    model.Deletedate = Convert.ToDateTime(DateTime.Now);
                    model.IsDelete = true;
                    unitOfWork.Save();
                }
                 else
                {
                    logger.Info("Error input data for Delete info unsuccessful!!!");
                }
                return Redirect(Request.UrlReferrer.ToString());
            }
            catch (Exception ex)
            {
             
                ModelState.AddModelError("", ex.Message);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        /// <summary>
        /// Info table Project
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Details(string id )
        {
            var idUser = User.Identity.GetUserId().ToString();
            var model = unitOfWork.Repository<ManageProjects>().GetbyID(id);
            listUserJoinProjectById(id);
            var tmp = unitOfWork.Repository<UserProject>().Get().Where(x => x.Project == id && x.Disable == false).ToList();
            ViewBag.tmp = tmp;
            return View(model);
            
        }
        /// <summary>
        ///  Set listUser by ID
        /// </summary>
        /// <param name="id"></param>
        public void listUserJoinProjectById(string id)
        {
            List<string> listUserNameJoinProject = new List<string>();
            var listUserIdJoinProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.Project == id && m.IsDelete == false && m.Disable == false).Select(n => n.UserId).ToList();
            foreach (var item in listUserIdJoinProject)
            {
                if (db.Users.Where(m => m.Id == item).Count() > 0)
                {
                    listUserNameJoinProject.Add((db.Users.Where(m => m.Id == item).Select(m => m.UserName)).SingleOrDefault());
                }

            }
            ViewBag.UserJoinProject_Name = listUserNameJoinProject;
        }
        /// <summary>
        /// Edit Member  - for Project by Id user
        /// </summary>
        /// <param name="ProjectId"></param>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult EditMember(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || (ManagerRolesController.IsDenied(IdUser, id) && User.IsInRole("PM")))
            {
                var model = unitOfWork.Repository<ManageProjects>().GetbyID(id);
                List<ApplicationUser> listTmp = db.Users.ToList();
                ViewBag.listUser = listTmp;
                ViewBag.UserNoIsProject = ((from b in db.Users select b.Id).Except(from c in unitOfWork.Repository<UserProject>().Get() where c.Project == id select c.UserId)).ToList();
                return View(model);
            }
            else
            {
                SetAlert("Bạn không có quyền !", "error");
                return RedirectToAction("EditMember");
            }
        }
        /// <summary>
        /// Delete Memer  in Project by Id
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="ProjectId"></param>
        /// <returns></returns>
        public ActionResult DeleteMember(string UserName,string id)
        {
            try
            {
                var userId = db.Users.Where(x => x.UserName == UserName).Select(m => m.Id).SingleOrDefault();
                var model = unitOfWork.Repository<UserProject>().Get().Where(x => x.UserId == userId && x.Project == id).SingleOrDefault();
                model.Disable = true;
                unitOfWork.Save();
                SetAlert("Update Success!", "success");
                return RedirectToAction("EditMember", new { id = id });
            }
            catch (Exception)
            {
                SetAlert("Update Error!", "error");
                return RedirectToAction("EditMember", new { id = id });
            }
           
        }
        /// <summary>
        /// Add Member 
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="ProjectId"></param>
        /// <returns></returns>
        public ActionResult AddMember(string UserName, string id, string IdUser)
        {
            var userId = db.Users.Where(x => x.UserName == UserName).Select(m => m.Id).SingleOrDefault();
            ViewBag.Task = new SelectList(db.Roles.Where(x => x.Name != "Admin").ToList(), "Name", "Name");

            return RedirectToAction("Edit","ManageUserProject", new { id = id, UserId=userId  ,IdUser=IdUser});
        }
        /// <summary>
        /// Set Alert for Update data
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }


        /// <summary>
        /// Change status for Properties table ManagerProject
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        
        [HttpPost]
        public void ChangeStatus(string  id ,string status)
        {
            try
            {
                var project = unitOfWork.Repository<ManageProjects>().GetbyID(id);
                project.isJoin = status;
                unitOfWork.Save();
            }
            catch (Exception)
            {
                logger.Error("Change status no success");
            }
          
        }

        ////InfoEnviroment
        public ActionResult InfoEnvironment(string IdProject, string IdE=null)
        {
            ViewBag.nameProject = unitOfWork.Repository<ManageProjects>().Get().Where(x => x.Project == IdProject && x.IsDelete == false).Select(x => x.ProjectName).FirstOrDefault();

            if (IdE == null)
            {
                var tmp = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == IdProject).Select(x => x.Environment).FirstOrDefault();
                IdE = tmp;
            }
            
            var query = unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.Projects.Project == IdProject && s.IsDelete == false && s.Environments.IsDelete == false, includeProperties: "Environments,Projects").Select(s => s.Environments).Distinct();
            ViewBag.Enviroment = query.ToList();
            ViewBag.tmp = unitOfWork.Repository<Servers>().Get().Where(s => s.IsDelete == false).ToList();
            if(IdE == null)
            {
                var tmp = unitOfWork.Repository<ProjectEnvironments>().Get().Where(e => e.IsDelete == false && e.Project == IdProject).Select(e => e.Environments.Environment).FirstOrDefault();
                IdE = tmp;
            }
            
            var a = unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.Project == IdProject && s.Environment == IdE && s.APIs.IsDelete == false && s.IsDelete == false, includeProperties: "APIs").Distinct();
            var w = unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.Project == IdProject && s.Environment == IdE && s.Websites.IsDelete == false && s.IsDelete == false, includeProperties: "Websites").Distinct();
            var d = unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.Project == IdProject && s.Environment == IdE && s.Databases.IsDelete == false && s.IsDelete == false, includeProperties: "Databases").Distinct();
            var p = unitOfWork.Repository<ProjectEnvironments>().Get(filter: s => s.Project == IdProject && s.Environment == IdE && s.Protocols.IsDelete == false && s.IsDelete == false, includeProperties: "Protocols").Distinct();
            var se = unitOfWork.Repository<ServerEnvironments>().Get(filter: s => s.Environment == IdE, includeProperties: "Environments.ProjectEnvironments").Distinct();

            ViewBag.APIs = a.ToList();
            ViewBag.Website = w.ToList();
            ViewBag.Database = d.ToList();
            ViewBag.countDb = d.ToList().Count() +1;
            ViewBag.Protocol = p.ToList();
            ViewBag.countProtocol = p.ToList().Count() +1;
            ViewBag.Server = se.Where(x => x.IsDelete == false && x.Servers.IsDelete == false && x.Environments.IsDelete == false).ToList();
            ViewBag.countServer = se.Where(x => x.IsDelete == false && x.Servers.IsDelete == false && x.Environments.IsDelete==false).ToList().Count()+1;
            
            ViewBag.IdProject = IdProject;
            ViewBag.Ide = IdE;
            return View( new {});
        }

        /// Info Bitbucket

        public ActionResult Bitbucket(string IdProject)
        {
            var x = unitOfWork.Repository<Bitbuckets>().Get().Where(p => p.Project == IdProject && p.IsDelete == false).ToList();
            ViewBag.idProject = IdProject;
            return View(x);
        }
        public ActionResult DetailsBbk(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
            if (bitbuckets == null)
            {
                return HttpNotFound();
            }
            return View(bitbuckets);
        }
        public ActionResult CreateBbk(string ProjectId)
        {
            var t = unitOfWork.Repository<ManageProjects>().Get();
            if (User.IsInRole("Admin"))
            {
                if (ProjectId == "undefined")
                {
                    ViewBag.tmp = t.Where(x => x.IsDelete == false);
                }
                else
                {
                    ViewBag.tmp = t.Where(x => x.Project == ProjectId && x.IsDelete == false);
                }
            }
            else
            {
                var id = User.Identity.GetUserId().ToString();
                var bitbucket = unitOfWork.Repository<ManageProjects>().Get(includeProperties: "UserProjects").Distinct();
                ViewBag.tmp = t.Where(x => x.Project == ProjectId && x.IsDelete == false);
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateBbk([Bind(Include = "Bitbucket,Project,Link,InfoBitbucket,Createby,Createdate")] Bitbuckets bitbuckets)
        {

            if (ModelState.IsValid)
            {
                bitbuckets.Createby = User.Identity.Name;
                bitbuckets.Createdate = Convert.ToDateTime(DateTime.Now);
                bitbuckets.Bitbucket = Guid.NewGuid().ToString();
                unitOfWork.Repository<Bitbuckets>().Insert(bitbuckets);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());

            }
            ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get(), "ProjectId", "ProjectName", bitbuckets.Project);
            SetAlert("Update Error", "error");
            return View(bitbuckets);
        }
        public ActionResult EditBbk(string id, string IdUser, string ProjectId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
                if (bitbuckets == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(x => x.Project == ProjectId).ToList(), "Project", "ProjectName", bitbuckets.Project);
                return View(bitbuckets);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBbk([Bind(Include = "Bitbucket,Project,Link,InfoBitbucket,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Bitbuckets bitbuckets)
        {
            if (ModelState.IsValid)
            {
                bitbuckets.Updateby = User.Identity.Name;
                bitbuckets.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Bitbuckets>().Update(bitbuckets);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");


                return Redirect(Request.UrlReferrer.ToString());
            }
            ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get(), "Project", "ProjectName", bitbuckets.Project);
            SetAlert("Update Error", "error");
            return View(bitbuckets);
        }
        public ActionResult DeleteBbk(string id, string IdUser, string ProjectId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
                if (bitbuckets == null)
                {
                    return HttpNotFound();
                }
                return View(bitbuckets);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }
        [HttpPost, ActionName("DeleteBbk")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
            bitbuckets.Deleteby = User.Identity.Name;
            bitbuckets.Deletedate = Convert.ToDateTime(DateTime.Now);
            bitbuckets.IsDelete = true;
            SetAlert("Update Successfully", "success");
            unitOfWork.Save();
            return Redirect(Request.UrlReferrer.ToString());
        }
        /// Ajax Delete Project
        /// 
        public JsonResult DeleteProject(string id)
        {
            var model = unitOfWork.Repository<ManageProjects>().GetbyID(id);
            bool status = false;
            try
            {
                if (ModelState.IsValid)
                {
                    model.Deleteby = User.Identity.Name;
                    model.Deletedate = Convert.ToDateTime(DateTime.Now);
                    model.IsDelete = true;
                    unitOfWork.Save();
                    status = true;
                }
                else
                {
                    logger.Info("Error input data for Delete info unsuccessful!!!");
                }
               
                return Json(status);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return Json(status);
        }


        // API //
        public ActionResult DetailsAPI(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
            if (aPIs == null)
            {
                return HttpNotFound();
            }
            return View(aPIs);
        }
        // Add Api
        public ActionResult AddExistingAPI(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdAPI = new SelectList(unitOfWork.Repository<APIs>().Get().Where(d => d.IsDelete == false), "API", "InfoAPI");
            }
            return View(); 
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddExistingAPI([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdAPI == projectEnvironments.IdAPI).ToList();
            var t = isTask.SingleOrDefault();
            if (isTask.Count > 0 && t.IsDelete == false)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }

        public ActionResult CreateAPI(string IdUser, string IdEnvironment,string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                if (IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
                items.Add(new SelectListItem() { Text = "External ", Value = "External" });

                ViewBag.Type = items.ToList();

            }
            else
            {
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ViewBag.tmp = y.ToList();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAPI([Bind(Include = "API,InfoAPI,UserName,Password,Type")] APIs aPIs, string IdEnvironment, string IdProject)
        {

            if (ModelState.IsValid)
            {
                aPIs.Createby = User.Identity.Name;
                aPIs.Createdate = Convert.ToDateTime(DateTime.Now);
                aPIs.API = Guid.NewGuid().ToString();
                unitOfWork.Repository<APIs>().Insert(aPIs);

                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdAPI = aPIs.API,
                        Createdate = aPIs.Createdate,
                        Createby = aPIs.Createby
                    };
                    aPIs.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                return Redirect(Request.UrlReferrer.ToString());
            }

            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
            items.Add(new SelectListItem() { Text = "External ", Value = "External" });

            ViewBag.Type = items.ToList();
            return View(aPIs);
        }

        public ActionResult EditAPI(string id, string IdUser, string EnvironmentId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
                if (aPIs == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
                items.Add(new SelectListItem() { Text = "External ", Value = "External" });
                ViewBag.Type = items.ToList();
                return View(aPIs);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAPI([Bind(Include = "API,InfoAPI,UserName,Password,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] APIs aPIs)
        {
            if (ModelState.IsValid)
            {

                aPIs.Updateby = User.Identity.Name;
                aPIs.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<APIs>().Update(aPIs);
                unitOfWork.Save();
                return Redirect(Request.UrlReferrer.ToString());
            }
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<ProjectEnvironments>().Get(), "IdEnviroment", "ProjectId"/*, aPIs.ProjectEnvId*/);
            return View(aPIs);
        }
        public JsonResult DeleteAPI(string id, string IdUser, string ProjectId, string EnvironmentId)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.Project == ProjectId && s.Environment == EnvironmentId && s.IdAPI == id).SingleOrDefault();
            bool status = false;
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return Json(status);
                }
                APIs aPIs = unitOfWork.Repository<APIs>().GetbyID(id);
                if (aPIs == null)
                {
                    return Json(status);
                }
                else
                {
                    if (isTask != null && isTask.IsDelete == false)
                    {
                        isTask.IsDelete = true;
                        isTask.Deleteby = User.Identity.Name;
                        isTask.Deletedate = Convert.ToDateTime(DateTime.Now);
                        unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                        unitOfWork.Save();
                    }
                    status = true;
                    return Json(status);
                }

            }
            return Json(status);
        }
        // Protocol //
        public ActionResult DetailsProtocol(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
            if (protocols == null)
            {
                return HttpNotFound();
            }
            return View(protocols);
        }
        // Add Protocol
        public ActionResult AddExistingProtocol(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdProtocol = new SelectList(unitOfWork.Repository<Protocols>().Get().Where(d => d.IsDelete == false), "Protocol", "HostName");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddExistingProtocol([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdProtocol == projectEnvironments.IdProtocol).ToList();
            var t = isTask.SingleOrDefault();
            if (isTask.Count > 0 && t.IsDelete == false)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }
        public ActionResult CreateProtocol(string IdUser, string IdEnvironment,string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                if (IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Sftp", Value = "Sftp" });
                items.Add(new SelectListItem() { Text = "Ftps", Value = "Ftps" });
                items.Add(new SelectListItem() { Text = "Ftp", Value = "Ftp" });
                ViewBag.Type = items.ToList();
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ApplicationUser model = db.Users.Find(IdUser);
                ViewBag.tmp = y.ToList();
            }

            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProtocol([Bind(Include = "Protocol,Type,HostName,Port,UserName,Password")] Protocols protocols, string IdEnvironment, string IdProject)
        {
            if (ModelState.IsValid)
            {
                protocols.Createby = User.Identity.Name;
                protocols.Createdate = Convert.ToDateTime(DateTime.Now);
                protocols.Protocol = Guid.NewGuid().ToString();
                unitOfWork.Repository<Protocols>().Insert(protocols);
                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdProtocol = protocols.Protocol,
                        Createdate = protocols.Createdate,
                        Createby = protocols.Createby
                    };
                    protocols.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully!", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }

            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, protocols.ProjectEnvId*/);
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Sftp", Value = "Sftp" });
            items.Add(new SelectListItem() { Text = "Ftps", Value = "Ftps" });
            items.Add(new SelectListItem() { Text = "Ftp", Value = "Ftp" });
            ViewBag.Type = items.ToList();
            SetAlert("Update Error!", "error");
            return View(protocols);
        }
        public ActionResult EditProtocol(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
                if (protocols == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();

                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Sftp", Value = "Sftp" });
                items.Add(new SelectListItem() { Text = "Ftps", Value = "Ftps" });
                items.Add(new SelectListItem() { Text = "Ftp", Value = "Ftp" });
                ViewBag.Type = items.ToList();
                return View(protocols);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProtocol([Bind(Include = "Protocol,Type,HostName,Port,UserName,Password,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Protocols protocols)
        {
            if (ModelState.IsValid)
            {
                protocols.Updateby = User.Identity.Name;
                protocols.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Protocols>().Update(protocols);
                unitOfWork.Save();
                SetAlert("Update Successfully!", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error!", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, protocols.ProjectEnvId*/);

            return View(protocols);
        }
        public ActionResult DeleteProtocol(string id)
        {
            bool status = false;
            try
            {
                var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdProtocol == id).SingleOrDefault();
                if (isTask != null && isTask.IsDelete == false)
                {
                    isTask.IsDelete = true;
                    isTask.Deleteby = User.Identity.Name;
                    isTask.Deletedate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                    unitOfWork.Save();
                }
                SetAlert("Update Successfully!", "success");
                status = true;
            }
            catch (Exception)
            {
                SetAlert("Update Error!", "error");
            }

            return Json(status);

        }
        // Website //
        public ActionResult DetailsWebsite(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
            if (websites == null)
            {
                return HttpNotFound();
            }
            return View(websites);
        }
        // Add Website
        public ActionResult AddExistingWebsite(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdWebsite = new SelectList(unitOfWork.Repository<Websites>().Get().Where(d => d.IsDelete == false), "Website", "Name");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddExistingWebsite([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdWebsite == projectEnvironments.IdWebsite).ToList();
            var t = isTask.SingleOrDefault();
            if (isTask.Count > 0 && t.IsDelete == false)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }
        public ActionResult CreateWebsite(string IdUser, string IdEnvironment,string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                if (IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ViewBag.tmp = y.ToList();
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWebsite([Bind(Include = "Website,Name,InfoWebsite")] Websites websites, string IdEnvironment, string IdProject)
        {
            if (ModelState.IsValid)
            {
                websites.Createdate = Convert.ToDateTime(DateTime.Now);
                websites.Createby = User.Identity.Name.ToString();
                websites.Website = Guid.NewGuid().ToString();
                unitOfWork.Repository<Websites>().Insert(websites);

                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdWebsite = websites.Website,
                        Createdate = websites.Createdate,
                        Createby = websites.Createby
                    };
                    websites.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, websites.ProjectEnvId*/);
            return View(websites);
        }

        public ActionResult EditWebsite(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Websites websites = unitOfWork.Repository<Websites>().GetbyID(id);
                if (websites == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                return View(websites);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditWebsite([Bind(Include = "Website,Name,InfoWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Websites websites)
        {
            if (ModelState.IsValid)
            {
                websites.Updateby = User.Identity.GetUserName().ToString();
                websites.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Websites>().Update(websites);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, websites.ProjectEnvId*/);
            return View(websites);
        }
        public JsonResult DeleteWebsite(string id)
        {
            bool status = false;
            try
            {
                var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdWebsite == id).SingleOrDefault();
                if (isTask != null && isTask.IsDelete == false)
                {
                    isTask.IsDelete = true;
                    isTask.Deleteby = User.Identity.Name;
                    isTask.Deletedate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                    unitOfWork.Save();
                }
                SetAlert("Update Successfully", "success");

                status = true;
            }
            catch (Exception)
            {
                SetAlert("Update Error", "error");
            }

            return Json(status);
        }
        // Database //
        public ActionResult DetailsDB(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
            if (databases == null)
            {
                return HttpNotFound();
            }
            return View(databases);
        }
        // Add database
        public ActionResult AddExistingDatabase(string IdUser, string IdEnvironment, string IdProject)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdProject))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", IdProject);
                ViewBag.IdDatabase = new SelectList(unitOfWork.Repository<Databases>().Get().Where(d => d.IsDelete == false), "Database", "DatabaseName");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddExistingDatabase([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.IdDatabase == projectEnvironments.IdDatabase).ToList();
            var t = isTask.SingleOrDefault();
            if (isTask.Count > 0 && t.IsDelete == false)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            return View(projectEnvironments);
        }
        public ActionResult CreateDB(string IdUser, string IdEnvironment)
        {
            if (User.IsInRole("Admin"))
            {
                if (IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ApplicationUser model = db.Users.Find(IdUser);
                ViewBag.tmp = y.ToList();
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDB([Bind(Include = "Database,InstanceName,DatabaseName,UserName,Password")] Databases databases, string IdEnvironment, string IdProject)
        {
            if (ModelState.IsValid)
            {
                databases.Createby = User.Identity.Name;
                databases.Createdate = Convert.ToDateTime(DateTime.Now);
                databases.Database = Guid.NewGuid().ToString();
                unitOfWork.Repository<Databases>().Insert(databases);
                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdDatabase = databases.Database,
                        Createdate = databases.Createdate,
                        Createby = databases.Createby
                    };
                    databases.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, databases.ProjectEnvId*/);

            return View(databases);
        }
        public ActionResult EditDB(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Databases databases = unitOfWork.Repository<Databases>().GetbyID(id);
                if (databases == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();

                return View(databases);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDB([Bind(Include = "Database,InstanceName,DatabaseName,UserName,Password,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Databases databases)
        {
            if (ModelState.IsValid)
            {
                databases.Updateby = User.Identity.Name;
                databases.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Databases>().Update(databases);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId");
            SetAlert("Update Error", "error");

            return View(databases);
        }
        public JsonResult DeleteDatabase(string id)
        {
            bool status = false;
            try
            {
                var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdDatabase == id).SingleOrDefault();
                if (isTask != null && isTask.IsDelete == false)
                {
                    isTask.IsDelete = true;
                    isTask.Deleteby = User.Identity.Name;
                    isTask.Deletedate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                    unitOfWork.Save();
                }
                status = true;
            }
            catch (Exception)
            {

            }

            return Json(status);
        }

        // ProjectEnvironment //
        public ActionResult CreateProEnv(string IdUser, string ProjectId)
        {

            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "Environment", "Type");
                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "Project", "ProjectName", ProjectId);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProEnv([Bind(Include = "Id,Project,Environment,IdAPI,IdProtocol,IdDatabase,IdWebsite,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ProjectEnvironments projectEnvironments)
        {
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.Project == projectEnvironments.Project && x.Environment == projectEnvironments.Environment).ToList();
            if (isTask.Count > 0)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            var t = isTask.SingleOrDefault();
            if (t != null && t.IsDelete == true)
            {
                unitOfWork.Repository<ProjectEnvironments>().Delete(t.Id);
                projectEnvironments.Id = t.Id;
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t != null)
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            else if (t == null && ModelState.IsValid)
            {
                projectEnvironments.Id = Guid.NewGuid().ToString();
                projectEnvironments.Createby = User.Identity.Name;
                projectEnvironments.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ProjectEnvironments>().Insert(projectEnvironments);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error: Value is already exist or value is error !", "error");

            ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(p => p.IsDelete == false), "IdEnviroment", "Type", projectEnvironments.Environment);
            ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(p => p.IsDelete == false), "ProjectId", "ProjectName", projectEnvironments.Project);
            return View(projectEnvironments);
        }
        public ActionResult DeleteEnvironment(string IdUser, string ProjectEnvironmentId, string ProjectId, string EnvironmentId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (ProjectId != null && EnvironmentId != null)
                {
                    db.ProjectEnvironments.RemoveRange(db.ProjectEnvironments.Where(s => s.Project == ProjectId && s.Environment == EnvironmentId));
                    db.SaveChanges();
                }

            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // ServerEnvironment // 
        public ActionResult DetailsSerEnv(string IdEnvironment, string IdServer)
        {
            if (IdEnvironment == null || IdServer == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
            if (serverEnvironment == null)
            {
                return HttpNotFound();
            }
            return View(serverEnvironment);
        }
        public ActionResult EditSerEnv(string IdUser, string IdServer, string IdEnvironment)
        {

            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (IdServer == null || IdEnvironment == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
                if (serverEnvironment == null)
                {
                    return HttpNotFound();
                }
                var query = from e in unitOfWork.Repository<ProjectEnvironments>().Get()
                            from p in unitOfWork.Repository<ManageProjects>().Get()
                            where e.Project == p.Project && e.IsDelete == false && p.IsDelete == false
                            select new { e.Environment, p.Project, p.ProjectName };
                ViewBag.IdEnviroment = new SelectList(query, "IdEnviroment", "ProjectName", serverEnvironment.Environment);
                ViewBag.IdServer = new SelectList(unitOfWork.Repository<Servers>().Get().Where(x => x.IsDelete == false), "IdServer", "ServerName");
                return View(serverEnvironment);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSerEnv([Bind(Include = "Server,Environment,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ServerEnvironments serverEnvironment)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    serverEnvironment.Updateby = User.Identity.Name;
                    serverEnvironment.Updatedate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<ServerEnvironments>().Update(serverEnvironment);
                    unitOfWork.Save();
                    SetAlert("Update Successfully", "success");
                    return Redirect(Request.UrlReferrer.ToString());

                }
            }
            catch
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId", serverEnvironment.Environment);
            ViewBag.IdServer = new SelectList(unitOfWork.Repository<Servers>().Get(), "IdServer", "IdEnviroment", serverEnvironment.Server);
            return View(serverEnvironment);
        }
        public ActionResult DeleteSerEnv(string IdUser, string IdServer, string IdEnvironment)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (IdServer == null && IdEnvironment == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
                if (serverEnvironment == null)
                {
                    return HttpNotFound();
                }
                return View(serverEnvironment);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }
        
        [HttpPost, ActionName("DeleteSerEnv")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string IdUser, string IdServer, string IdEnvironment)
        {
            ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
            serverEnvironment.Deleteby = User.Identity.Name;
            serverEnvironment.Deletedate = Convert.ToDateTime(DateTime.Now);
            serverEnvironment.IsDelete = true;
            SetAlert("Update Successfully", "success");
            unitOfWork.Save();

            return Redirect(Request.UrlReferrer.ToString());
        }
        public ActionResult CreateSerEnv(string IdUser, string IdEnvironment)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(x => x.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Server = new SelectList(unitOfWork.Repository<Servers>().Get().Where(x => x.IsDelete == false), "Server", "ServerName");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSerEnv([Bind(Include = "Environment,Server,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ServerEnvironments serverEnvironment)
        {
            var isTask = unitOfWork.Repository<ServerEnvironments>().Get().Where(x => x.Server == serverEnvironment.Server && x.Environment == serverEnvironment.Environment).SingleOrDefault();
            if (isTask != null && isTask.IsDelete == true)
            {
                unitOfWork.Repository<ServerEnvironments>().Delete2(isTask.Environment, isTask.Server);
                serverEnvironment.Createby = User.Identity.Name;
                serverEnvironment.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ServerEnvironments>().Insert(serverEnvironment);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            try
            {
                if (ModelState.IsValid)
                {
                    serverEnvironment.Createby = User.Identity.Name;
                    serverEnvironment.Createdate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<ServerEnvironments>().Insert(serverEnvironment);
                    unitOfWork.Save();
                    SetAlert("Update Successfully", "success");

                    return Redirect(Request.UrlReferrer.ToString());
                }
            }
            catch
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }

            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "Environment", "Project", serverEnvironment.Environment);
            ViewBag.IdServer = new SelectList(unitOfWork.Repository<Servers>().Get(), "Server", "Environment", serverEnvironment.Server);
            return View(serverEnvironment);
        }
        // Server //
        public ActionResult CreateSer(string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                ViewBag.tmp = unitOfWork.Repository<Enviroment>().Get().Where(x => x.IsDelete == false).ToList();
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
                items.Add(new SelectListItem() { Text = "External ", Value = "External" });

                ViewBag.Type = items.ToList();
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ViewBag.tmp = y.ToList();
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSer([Bind(Include = "Server,ServerName,Type,Location,InfoServer")] Servers servers, string IdEnvironment)
        {
            if (ModelState.IsValid)
            {
                servers.Createby = User.Identity.Name.ToString();
                servers.Createdate = Convert.ToDateTime(DateTime.Now);
                servers.Server = Guid.NewGuid().ToString();
                unitOfWork.Repository<Servers>().Insert(servers);

                if (IdEnvironment != null)
                {
                    var serverEnv = new ServerEnvironments
                    {
                        Environment = IdEnvironment,
                        Server = servers.Server,
                        Createby = servers.Createby,
                        Createdate = servers.Createdate
                    };
                    servers.ServerEnvironments.Add(serverEnv);

                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            return View(servers);
        }
    }
}
