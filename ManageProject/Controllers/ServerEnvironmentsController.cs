﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;

namespace ManageProject.Controllers
{
    public class ServerEnvironmentsController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: ServerEnvironments
        public ActionResult Index()
        {
            var serverEnvironments = unitOfWork.Repository<ServerEnvironments>().Get().Where(s => s.IsDelete == false && s.Servers.IsDelete == false && s.Environments.IsDelete == false);
            return View(serverEnvironments.ToList());
        }

        // GET: ServerEnvironments/Details/5
        public ActionResult Details(string IdEnvironment, string IdServer)
        {
            if (IdEnvironment == null || IdServer == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
            if (serverEnvironment == null)
            {
                return HttpNotFound();
            }
            return View(serverEnvironment);
        }

        // GET: ServerEnvironments/Create
        public ActionResult Create(string IdUser,string IdEnvironment)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                ViewBag.Environment = new SelectList(unitOfWork.Repository<Enviroment>().Get().Where(x => x.IsDelete == false), "Environment", "Type", IdEnvironment);
                ViewBag.Server = new SelectList(unitOfWork.Repository<Servers>().Get().Where(x => x.IsDelete == false), "Server", "ServerName");
            }
            
            return View();
        }

        // POST: ServerEnvironments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Environment,Server,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ServerEnvironments serverEnvironment)
        {
            var isTask = unitOfWork.Repository<ServerEnvironments>().Get().Where(x => x.Server == serverEnvironment.Server && x.Environment == serverEnvironment.Environment).SingleOrDefault();
            if(isTask != null && isTask.IsDelete == true)
            {
                unitOfWork.Repository<ServerEnvironments>().Delete2(isTask.Environment, isTask.Server);
                serverEnvironment.Createby = User.Identity.Name;
                serverEnvironment.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ServerEnvironments>().Insert(serverEnvironment);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            try
            {
                if (ModelState.IsValid)
            {
                serverEnvironment.Createby = User.Identity.Name;
                serverEnvironment.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<ServerEnvironments>().Insert(serverEnvironment);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            }
            catch
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "Environment", "Project", serverEnvironment.Environment);
            ViewBag.IdServer = new SelectList(unitOfWork.Repository<Servers>().Get(), "Server", "Environment", serverEnvironment.Server);
            return View(serverEnvironment);
        }

        // GET: ServerEnvironments/Edit/5
        public ActionResult Edit( string IdUser, string IdServer,string IdEnvironment)
        {
            
            if (User.IsInRole("Admin"))
            {
                if (IdServer == null || IdEnvironment==null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
                if (serverEnvironment == null)
                {
                    return HttpNotFound();
                }
                var query = from e in unitOfWork.Repository<ProjectEnvironments>().Get()
                            from p in unitOfWork.Repository<ManageProjects>().Get()
                            where e.Project == p.Project && e.IsDelete == false && p.IsDelete == false
                            select new { e.Environment, p.Project, p.ProjectName };
                ViewBag.IdEnviroment = new SelectList(query, "IdEnviroment", "ProjectName", serverEnvironment.Environment);
                ViewBag.IdServer = new SelectList(unitOfWork.Repository<Servers>().Get().Where(x => x.IsDelete == false), "IdServer", "ServerName");
                return View(serverEnvironment);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: ServerEnvironments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Server,Environment,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] ServerEnvironments serverEnvironment)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    serverEnvironment.Updateby = User.Identity.Name;
                    serverEnvironment.Updatedate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<ServerEnvironments>().Update(serverEnvironment);
                    unitOfWork.Save();
                    SetAlert("Update Successfully", "success");
                    return Redirect(Request.UrlReferrer.ToString());

                }
            }
            catch
            {
                SetAlert("Update Error: Value is already exist or value is error !", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
            
            SetAlert("Update Error", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId", serverEnvironment.Environment);
            ViewBag.IdServer = new SelectList(unitOfWork.Repository<Servers>().Get(), "IdServer", "IdEnviroment", serverEnvironment.Server);
            return View(serverEnvironment);
        }

        // GET: ServerEnvironments/Delete/5
        public ActionResult Delete(string IdUser, string IdServer,string IdEnvironment)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, IdServer))
            {
                if (IdServer == null && IdEnvironment==null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
                if (serverEnvironment == null)
                {
                    return HttpNotFound();
                }
                return View(serverEnvironment);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        // POST: ServerEnvironments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string IdUser, string IdServer, string IdEnvironment)
        {
            ServerEnvironments serverEnvironment = unitOfWork.Repository<ServerEnvironments>().GetbyID2(IdEnvironment, IdServer);
            serverEnvironment.Deleteby = User.Identity.Name;
            serverEnvironment.Deletedate = Convert.ToDateTime(DateTime.Now);
            serverEnvironment.IsDelete = true;
            SetAlert("Update Successfully", "success");
            unitOfWork.Save();

            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
