﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ManageProject.Controllers
{
    public class ProtocolsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Protocols
        public ActionResult Index()
        {
            var protocols = unitOfWork.Repository<Protocols>().Get().Where(x => x.IsDelete == false);
            return View(protocols.ToList());
        }

        // GET: Protocols/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
            if (protocols == null)
            {
                return HttpNotFound();
            }
            return View(protocols);
        }

        // GET: Protocols/Create
        public ActionResult Create(string IdUser,string IdEnvironment)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Sftp", Value = "Sftp" });
            items.Add(new SelectListItem() { Text = "Ftps", Value = "Ftps" });
            items.Add(new SelectListItem() { Text = "Ftp", Value = "Ftp" });
            ViewBag.Type = items.ToList();
            if (User.IsInRole("Admin"))
            {
                if(IdEnvironment != null)
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false && x.Environment == IdEnvironment).ToList();                
                }
                else
                {
                    ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                }
                
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ApplicationUser model = db.Users.Find(IdUser);
                ViewBag.tmp = y.ToList();
            }

            return View();
           
        }

        // POST: Protocols/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Protocol,Type,HostName,Port,UserName,Password")] Protocols protocols, string IdEnvironment, string IdProject)
        {
            if (ModelState.IsValid)
            {
                protocols.Createby = User.Identity.Name;
                protocols.Createdate = Convert.ToDateTime(DateTime.Now);
                protocols.Protocol = Guid.NewGuid().ToString();
                unitOfWork.Repository<Protocols>().Insert(protocols);
                if (IdEnvironment != null && IdProject != null)
                {
                    var ProjectEnv = new ProjectEnvironments
                    {
                        Id = Guid.NewGuid().ToString(),
                        Environment = IdEnvironment,
                        Project = IdProject,
                        IdProtocol = protocols.Protocol,
                        Createdate = protocols.Createdate,
                        Createby = protocols.Createby
                    };
                    protocols.ProjectEnvironments.Add(ProjectEnv);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully!", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }

            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, protocols.ProjectEnvId*/);
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Sftp", Value = "Sftp" });
            items.Add(new SelectListItem() { Text = "Ftps", Value = "Ftps" });
            items.Add(new SelectListItem() { Text = "Ftp", Value = "Ftp" });
            ViewBag.Type = items.ToList();
            SetAlert("Update Error!", "error");
            return View(protocols);
        }

        // GET: Protocols/Edit/5
        public ActionResult Edit(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
                if (protocols == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ProjectEnvId = unitOfWork.Repository<ProjectEnvironments>().Get().Where(x => x.IsDelete == false).ToList();
                
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Sftp", Value = "Sftp" });
                items.Add(new SelectListItem() { Text = "Ftps", Value = "Ftps" });
                items.Add(new SelectListItem() { Text = "Ftp", Value = "Ftp" });
                ViewBag.Type = items.ToList();
                return View(protocols);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Protocols/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Protocol,Type,HostName,Port,UserName,Password,Type,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Protocols protocols)
        {
            if (ModelState.IsValid)
            {
                protocols.Updateby = User.Identity.Name;
                protocols.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Protocols>().Update(protocols);
                unitOfWork.Save();
                SetAlert("Update Successfully!", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error!", "error");
            ViewBag.IdEnviroment = new SelectList(unitOfWork.Repository<Enviroment>().Get(), "IdEnviroment", "ProjectId"/*, protocols.ProjectEnvId*/);

            return View(protocols);
        }

        // GET: Protocols/Delete/5
        public ActionResult Delete(string id, string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
                if (protocols == null)
                {
                    return HttpNotFound();
                }
                return View(protocols);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Protocols/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
            protocols.Deleteby = User.Identity.Name;
            protocols.Deletedate = Convert.ToDateTime(DateTime.Now);
            protocols.IsDelete = true;
            var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdProtocol == id).SingleOrDefault();
            if (isTask != null && isTask.IsDelete == false)
            {
                isTask.IsDelete = true;
                isTask.Deleteby = protocols.Deleteby;
                isTask.Deletedate = protocols.Deletedate;
                unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                unitOfWork.Save();
            }
            unitOfWork.Save();
            SetAlert("Update Successfully!", "success");
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
       

        ///Ajax delete Protocol
        ///
        public ActionResult DeleteProtocol(string id)
        {
            bool status = false;
            try
            {
                Protocols protocols = unitOfWork.Repository<Protocols>().GetbyID(id);
                protocols.Deleteby = User.Identity.Name;
                protocols.Deletedate = Convert.ToDateTime(DateTime.Now);
                protocols.IsDelete = true;
                var isTask = unitOfWork.Repository<ProjectEnvironments>().Get().Where(s => s.IdProtocol == id).SingleOrDefault();
                if (isTask != null && isTask.IsDelete == false)
                {
                    isTask.IsDelete = true;
                    isTask.Deleteby = protocols.Deleteby;
                    isTask.Deletedate = protocols.Deletedate;
                    unitOfWork.Repository<ProjectEnvironments>().Update(isTask);
                }
                unitOfWork.Save();
                SetAlert("Update Successfully!", "success");
                status = true;
            }
            catch (Exception)
            {
                SetAlert("Update Error!", "error");
            }

            return Json(status);

        }
        [HttpPost]
        public JsonResult SendMail(string id, string IdUser)
        {
            bool isSend = false;
            try
            {
                var mailUser = User.Identity.GetUserName();
                var NameUser = db.Users.Where(x => x.Id == IdUser).Select(x => x.FullName).FirstOrDefault().ToString();
                
                var username = ConfigurationManager.AppSettings["userName"].ToString();
                var password = ConfigurationManager.AppSettings["userPassword"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
                var host = ConfigurationManager.AppSettings["Host"].ToString();
                var client = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(username, password),
                    EnableSsl = true,
                };
                var from = new MailAddress(username, "AdminTest");
                var to = new MailAddress(mailUser);
                var mail = new MailMessage(from, to)
                {
                    Subject = "Send protocol's password",
                    Body = createBody(id, NameUser),
                    IsBodyHtml = true,
                };
                client.Send(mail);
                isSend = true;
                return Json(new { isSend=isSend});
            }
            catch(Exception ex)
            {
                SetAlert("Update Error!", "error");
            }

            return Json(new { isSend = isSend });
        }
        public string createBody(string idProtocol,string nameUser)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Views/TemplateEmail/MailProtocol.cshtml")))

            {

                body = reader.ReadToEnd();

            }
            var tmp = unitOfWork.Repository<Protocols>().Get().Where(x => x.Protocol == idProtocol).FirstOrDefault();

            body = body.Replace("{FullName}", nameUser);
            body = body.Replace("{HostName}", tmp.HostName); //replacing the required things  

            body = body.Replace("{Port}", tmp.Port);

            body = body.Replace("{Type}", tmp.Type);
            body = body.Replace("{UserName}", tmp.UserName);
            body = body.Replace("{Password}", tmp.Password);
            body = body.Replace("{CreateDate}", tmp.Createdate.ToString());
            body = body.Replace("{UpdateDate}", tmp.Updatedate.ToString());
            return body;

        }
        /// <summary>
        /// Set Alert for Update data
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
