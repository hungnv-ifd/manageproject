﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;

namespace ManageProject.Controllers
{
    public class ServersController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: Servers
        public ActionResult Index()
        {
            var servers = unitOfWork.Repository<Servers>().Get().Where(x => x.IsDelete == false);
            return View(servers.ToList());
           
        }

        // GET: Servers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servers servers = unitOfWork.Repository<Servers>().GetbyID(id);
            if (servers == null)
            {
                return HttpNotFound();
            }
            return View(servers);
        }

        // GET: Servers/Create
        public ActionResult Create(string IdUser)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
            items.Add(new SelectListItem() { Text = "External ", Value = "External" });

            ViewBag.Type = items.ToList();
            if (User.IsInRole("Admin"))
            {
                ViewBag.tmp = unitOfWork.Repository<Enviroment>().Get().Where(x => x.IsDelete == false).ToList();  
            }
            else
            {
                var userProject = unitOfWork.Repository<UserProject>().Get().Where(m => m.UserId == IdUser).ToList();
                var y = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject.ProjectEnvironments").Select(s => s.managerProject.ProjectEnvironments).Distinct();
                ViewBag.tmp = y.ToList();
            }

            return View();
        }

        // POST: Servers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Server,ServerName,Type,Location,InfoServer")] Servers servers,string IdEnvironment)
        {
            if (ModelState.IsValid)
            {
                servers.Createby = User.Identity.Name.ToString();
                servers.Createdate = Convert.ToDateTime(DateTime.Now);
                servers.Server = Guid.NewGuid().ToString();
                unitOfWork.Repository<Servers>().Insert(servers);
                
                if(IdEnvironment != null)
                {
                    var serverEnv = new ServerEnvironments
                    {
                        Environment = IdEnvironment,
                        Server = servers.Server,
                        Createby = servers.Createby,
                        Createdate = servers.Createdate
                    };
                    servers.ServerEnvironments.Add(serverEnv);
                    
                }
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            return View(servers);
        }

        // GET: Servers/Edit/5
        public ActionResult Edit(string id,string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, id))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Servers servers = unitOfWork.Repository<Servers>().GetbyID(id);
                if (servers == null)
                {
                    return HttpNotFound();
                }

                ViewBag.tmp = unitOfWork.Repository<Enviroment>().Get().Where(x => x.IsDelete == false).ToList();
                var items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Text = "Internal", Value = "Internal" });
                items.Add(new SelectListItem() { Text = "External ", Value = "External" });

                ViewBag.Type = items.ToList();
                return View(servers);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Servers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Server,ServerName,Type,Location,InfoServer,Type,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Servers servers)
        {
            if (ModelState.IsValid)
            {
                servers.Updateby = User.Identity.Name;
                servers.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Servers>().Update(servers);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());
            }
            SetAlert("Update Error", "error");
            return View(servers);
        }

        // GET: Servers/Delete/5
        public ActionResult Delete(string id,string IdUser)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, id))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Servers servers = unitOfWork.Repository<Servers>().GetbyID(id);
                if (servers == null)
                {
                    return HttpNotFound();
                }
                return View(servers);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: Servers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Servers servers = unitOfWork.Repository<Servers>().GetbyID(id);
            servers.Deleteby = User.Identity.Name;
            servers.Deletedate = Convert.ToDateTime(DateTime.Now);
            servers.IsDelete = true;
            SetAlert("Update Successfully", "success");
            unitOfWork.Save();
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Ajax delete Server
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteServer(string id)
        {
            bool status = false;
            try
            {
                Servers servers = unitOfWork.Repository<Servers>().GetbyID(id);
                servers.Deleteby = User.Identity.Name;
                servers.Deletedate = Convert.ToDateTime(DateTime.Now);
                servers.IsDelete = true;
                SetAlert("Update Successfully", "success");
                unitOfWork.Save();
                status = true;
            }
            catch (Exception)
            {
                SetAlert("Update Error", "error");
            }

            return Json(status);
        }

        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
