﻿using log4net;
using ManageProject.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ManageProject.Controllers
{
    [Authorize(Roles ="Admin")]
    public class ManageUserRolesController : Controller
    {
        private static readonly ILog logger = LogManager.GetLogger(

         System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: ManageUser
        public ActionResult Index()
        {
            var model = db.Users.ToList();
            return View(model);
        }

        // GET: ManageUser/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ManageUser/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ManageUser/Create
        [HttpPost]
        public ActionResult Create(ApplicationUser user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Users.Add(user);
                }
                else
                {
                    logger.Info("Create Role fail!!");
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Lỗi dữ liệu :" + ex.Message);
                return View();
            }
        }

        // GET: ManageUser/Edit/5
        public ActionResult Edit(string id)
        {
            var model = db.Users.Find(id);
            return View(model);
        }
        // POST: ManageUser/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string Id, ApplicationUser user)
        {
            ApplicationUser model = null;
            try
            {
                model = db.Users.Find(Id);
                if (ModelState.IsValid)
                {
                    model.Phone = user.Phone;
                    model.EmailConfirmed = user.EmailConfirmed;
                    model.LockoutEnabled = user.LockoutEnabled;
                    db.SaveChanges();
                }
                else
                {
                    logger.Info(ModelState.IsValid.ToString());
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                logger.Error(ex.Message);
                ModelState.AddModelError("", "Lỗi dữ liệu : " + ex.Message);
            }
            return View(model);
        }

        // GET: ManageUser/Delete/5
        public ActionResult Delete(string id)
        {
            var model = db.Users.Find(id);
            return View(model);
        }

        // POST: ManageUser/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteUser(string Id)
        {
            try
            {
                var model = db.Users.Find(Id);
                db.Users.Remove(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                ModelState.AddModelError("", "Lỗi dữ liệu : " + ex.Message);
                return View();
            }
        }

        /// <summary>
        /// edit role for user, which can perform project 's action
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult EditRole(string Id)
        {
            ApplicationUser model = db.Users.Find(Id);
            ViewBag.NameRoles = db.Roles.ToList().Where(item => model.Roles.FirstOrDefault(x => x.RoleId == item.Id) != null).ToList();
            ViewBag.NameRoles2 = db.Roles.ToList().Where(item => model.Roles.FirstOrDefault(x => x.RoleId == item.Id) == null).ToList();
            return View(model);
        }
        /// <summary>
        /// add role for user, which can perform project 's action
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("EditRole")]
        public ActionResult AddRole(string UserId, string[] RoleId)
        {
            ApplicationUser model = db.Users.Find(UserId);
            if (RoleId != null && RoleId.Count() > 0)
            {
                foreach (var item in RoleId)
                {
                    IdentityRole role = db.Roles.Find(RoleId);
                    model.Roles.Add(new IdentityUserRole() { UserId = UserId, RoleId = item });
                }
                db.SaveChanges();
            }
            else
                {
               
                logger.Info("AddMember for User fail!!");
                }
            return RedirectToAction("EditRole", new { Id = UserId });
        }
        /// <summary>
        /// delete role for user, which can perform project 's action
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        [ActionName("DeleteRoles")]
        public ActionResult DeleteRole(string UserId, string RoleId)
        {
            ApplicationUser model = db.Users.Find(UserId);
            model.Roles.Remove(model.Roles.Single(m => m.RoleId == RoleId));
            db.SaveChanges();
            return RedirectToAction("EditRole", new { Id = UserId });
        }
    }
}