﻿using ManageProject.DAL;
using ManageProject.DAL.NUoW;
using ManageProject.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ManageProject.Controllers
{
    public class BitbucketsController : Controller
    {
        private GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: Bitbuckets
        public ActionResult Index()
        {
            var bitbuckets = unitOfWork.Repository<Bitbuckets>().Get().Where(x => x.IsDelete == false);
            return View(bitbuckets.ToList());
        }

        // GET: Bitbuckets/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
            if (bitbuckets == null)
            {
                return HttpNotFound();
            }
            return View(bitbuckets);
        }

        // GET: Bitbuckets/Create
        public ActionResult Create(string ProjectId)
        {

            if (User.IsInRole("Admin"))
            {
                if (ProjectId == "undefined")
                {
                    ViewBag.tmp = unitOfWork.Repository<ManageProjects>().Get().Where(x => x.IsDelete == false);
                }
                else
                {
                    ViewBag.tmp = unitOfWork.Repository<ManageProjects>().Get().Where(x => x.Project == ProjectId && x.IsDelete == false);
                }

            }
            else
            {
                var id = User.Identity.GetUserId().ToString();
                var bitbucket = unitOfWork.Repository<ManageProjects>().Get(includeProperties: "UserProjects").Distinct();
                ViewBag.tmp = bitbucket.ToList();
            }

            return View();
        }


        // POST: Bitbuckets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Bitbucket,Project,Link,InfoBitbucket,Createby,Createdate")] Bitbuckets bitbuckets)
        {

            if (ModelState.IsValid)
            {
                bitbuckets.Createby = User.Identity.Name;
                bitbuckets.Createdate = Convert.ToDateTime(DateTime.Now);
                bitbuckets.Bitbucket = Guid.NewGuid().ToString();
                unitOfWork.Repository<Bitbuckets>().Insert(bitbuckets);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");

                return Redirect(Request.UrlReferrer.ToString());

            }

            ViewBag.ProjectId = new SelectList(unitOfWork.Repository<ManageProjects>().Get(), "ProjectId", "ProjectName", bitbuckets.Project);
            SetAlert("Update Error", "error");

            return View(bitbuckets);
            
        }

        // GET: Bitbuckets/Edit/5
        public ActionResult Edit(string id, string IdUser, string ProjectId)
        {
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
                if (bitbuckets == null)
                {
                    return HttpNotFound();
                }

                ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(x => x.Project == ProjectId).ToList(), "Project", "ProjectName", bitbuckets.Project);
                return View(bitbuckets);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        // POST: Bitbuckets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Bitbucket,Project,Link,InfoBitbucket,Createdate,Createby,Deletedate,Deleteby,IsDelete")] Bitbuckets bitbuckets)
        {
            if (ModelState.IsValid)
            {
                bitbuckets.Updateby = User.Identity.Name;
                bitbuckets.Updatedate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<Bitbuckets>().Update(bitbuckets);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");


                return Redirect(Request.UrlReferrer.ToString());
            }
            ViewBag.Project = new SelectList(unitOfWork.Repository<ManageProjects>().Get(), "Project", "ProjectName", bitbuckets.Project);

            SetAlert("Update Error", "error");

            return View(bitbuckets);
        }

        // GET: Bitbuckets/Delete/5
        public ActionResult Delete(string id, string IdUser, string ProjectId)
        {
            
            if (User.IsInRole("Admin") || ManagerRolesController.IsDenied(IdUser, ProjectId))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);

                if (bitbuckets == null)
                {
                    return HttpNotFound();
                }
                return View(bitbuckets);
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        // POST: Bitbuckets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
            bitbuckets.Deleteby = User.Identity.Name;
            bitbuckets.Deletedate = Convert.ToDateTime(DateTime.Now);
            bitbuckets.IsDelete = true;
            SetAlert("Update Successfully", "success");
            unitOfWork.Save();
            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        ///Ajax delete bitbucket
        ///
        public ActionResult DeleteBitbucket(string id)
        {
            bool status = false;
            try
            {
                Bitbuckets bitbuckets = unitOfWork.Repository<Bitbuckets>().GetbyID(id);
                bitbuckets.Deleteby = User.Identity.Name;
                bitbuckets.Deletedate = Convert.ToDateTime(DateTime.Now);
                bitbuckets.IsDelete = true;
                unitOfWork.Save();
                status = true;
            }
            catch (Exception)
            {

                throw;
            }
            return Json(status);

        }
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }
    }
}
