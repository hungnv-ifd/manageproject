﻿using log4net;
using ManageProject.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ManageProject.DAL;
using ManageProject.DAL.NUoW;

namespace ManageProject.Controllers
{
    [Authorize(Roles = "Admin,PM")]
    public class ManageUserProjectController : Controller
    {
        private static readonly ILog logger = LogManager.GetLogger(

        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ApplicationDbContext db = new ApplicationDbContext();
        GenericUnitOfWork unitOfWork = new GenericUnitOfWork();
        // GET: ManageIProject
        public ActionResult Index()
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(x => x.Disable == false).ToList();
            return View(model);
        }
        public ActionResult Create(string IdUser)
        {
            if(User.IsInRole("Admin"))
            {
                ViewBag.ListProject = new SelectList(unitOfWork.Repository<ManageProjects>().Get().Where(x => x.IsDelete == false).ToList(), "Project", "ProjectName");
                ViewBag.ListUser = new SelectList(db.Users.ToList(),"Id", "UserName");
                ViewBag.Task = new SelectList(db.Roles.Where(x => x.Name != "Admin").ToList(), "Name", "Name");
                return View();
            }
            else if ((ManagerRolesController.IsDenied(IdUser)&& User.IsInRole("PM")))
            {
                var tmp = unitOfWork.Repository<UserProject>().Get(filter: s => s.UserId == IdUser, includeProperties: "managerProject").Select(s => s.managerProject).Distinct();
                ViewBag.ListProject = new SelectList(tmp.ToList(),"Project", "ProjectName");
                ViewBag.ListUser = new SelectList(db.Users.ToList(),"Id", "UserName");
                ViewBag.Task = new SelectList(db.Roles.Where(x=>x.Name!="Admin").ToList(), "Name", "Name");
                return View();
            }
            
            else
            {
                logger.Info("Create Task fail!!");
                SetAlert("Bạn không có quyền truy cập", "error");
               return RedirectToAction("Index");
            }
          
        }
        /// <summary>
        /// Create UserProject
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Project,UserId,Disable,TaskUser,PositionUser,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] UserProject userProject)
        {

            var isTask = unitOfWork.Repository<UserProject>().Get().Where(x => x.Project == userProject.Project && x.UserId == userProject.UserId).SingleOrDefault();
            if (isTask != null && isTask.Disable == true)
            {
                unitOfWork.Repository<UserProject>().Delete2(isTask.Project, isTask.UserId);
                userProject.Createby = User.Identity.Name;
                userProject.Createdate = Convert.ToDateTime(DateTime.Now);
                unitOfWork.Repository<UserProject>().Insert(userProject);
                unitOfWork.Save();
                SetAlert("Update Successfully", "success");
                return RedirectToAction("Index");
            }
            else if (isTask != null)
            {
                SetAlert("User is already exist!", "error");
                return Redirect(Request.UrlReferrer.ToString());
            }
                
            try
            {
                if (ModelState.IsValid)
                {
                    userProject.Createby = User.Identity.Name;
                    userProject.Createdate = Convert.ToDateTime(DateTime.Now);
                    unitOfWork.Repository<UserProject>().Insert(userProject);
                    unitOfWork.Save();
                    SetAlert("Update Successfully", "success");
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                SetAlert("Update Error", "error");
                logger.ErrorFormat("Errors:{0}", ex.Message);
                ModelState.AddModelError("", ex.Message);
            }
            return View(userProject);
        }
        /// <summary>
        /// Edit table UserProject
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ProjectId"></param>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult Edit(string UserId, string id,string IdUser)
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(x => x.Project == id && x.UserId == UserId).FirstOrDefault();
            ViewBag.Task = new SelectList(db.Roles.Where(x => x.Name != "Admin").ToList(), "Name", "Name");

            if (User.IsInRole("Admin") ||(User.IsInRole("PM") && ManagerRolesController.IsDenied(IdUser, id)) )
            {
                if(model == null)
                {
                    var userproject = new UserProject
                    {
                        Project = id,
                        UserId = UserId
                    };
                    return View(userproject);
                }
                return View(model);
            }
            else
            {
        
                SetAlert("Bạn không có quyền", "error");
                return RedirectToAction("Index");
            }
           
            
        }
        /// <summary>
        /// Edit table UserProject -method Post
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Project,UserId,Disable,TaskUser,PositionUser,Notes,Updatedate,Updateby,Createdate,Createby,Deletedate,Deleteby,IsDelete")] UserProject userProject)
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(x => x.Project == userProject.Project && x.UserId == userProject.UserId).SingleOrDefault();
            try
            {
                if (model !=null && ModelState.IsValid)
                {
                    model.Updateby = User.Identity.Name;
                    model.Updatedate = Convert.ToDateTime(DateTime.Now);
                    model.TaskUser = userProject.TaskUser;
                    model.PositionUser = userProject.PositionUser;
                    model.Disable = false;
                    unitOfWork.Save();
                }
                if(model==null)
                {
                    unitOfWork.Repository<UserProject>().Insert(userProject);
                    unitOfWork.Save();
                }
                SetAlert("Update successfully", "success");
                return Redirect(Request.UrlReferrer.ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                SetAlert("Update Error", "error");
                ModelState.AddModelError("", ex.Message);
            }
            return View(model);

        }

        /// <summary>
        /// Delete value table UserProject
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ProjectId"></param>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult Delete(string UserId,string ProjectId,string IdUser)
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(m => m.Project == ProjectId && m.UserId == UserId).SingleOrDefault();
            if (User.IsInRole("Admin")||(User.IsInRole("PM") && ManagerRolesController.IsDenied(IdUser,ProjectId)))
            {
                return View(model);
            }
            else
            {
                SetAlert("Bạn không có quyền!", "error");
                return RedirectToAction("Index");
            }
            
            
        }
        /// <summary>
        /// Delete User 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ProjectId"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult ComfirmDelete(string UserId, string ProjectId)
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(m => m.Project == ProjectId && m.UserId == UserId).SingleOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    model.Deleteby = User.Identity.Name;
                    model.Deletedate = Convert.ToDateTime(DateTime.Now);
                    model.IsDelete = true;
                    unitOfWork.Save();
                    SetAlert("Update success", "success");
                    return Redirect(Request.UrlReferrer.ToString());
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                SetAlert("Update Error", "error");
                ModelState.AddModelError("", ex.Message);
            }
            return RedirectToAction("Index");

        }

        /// <summary>
        /// Info table UserProject
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ProjectId"></param>
        /// <returns></returns>
        public ActionResult Details(string UserId, string id)
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(m => m.Project == id && m.UserId == UserId).SingleOrDefault();
            return View(model);

        }

        /// <summary>
        /// Info User
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public ActionResult InfoUser(string UserId)
        {
            var model = unitOfWork.Repository<UserProject>().Get().Where(x => x.UserId == UserId && x.Disable == false).ToList();
            return View(model);
        }
        /// <summary>
        /// Change status update data
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public void SetAlert(string message, string type)
        {

            TempData[""] = message;
            TempData["AlertMesage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warnig";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }

        }

        ///Ajax delete UserProject
        ///
        public JsonResult DeleteUserProject (string UserId, string id)
        {
            bool status = false;
            var model = unitOfWork.Repository<UserProject>().Get().Where(m => m.Project == id && m.UserId == UserId).SingleOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    model.Deleteby = User.Identity.Name;
                    model.Deletedate = Convert.ToDateTime(DateTime.Now);
                    model.IsDelete = true;
                    model.Disable = true;
                    unitOfWork.Save();
                    SetAlert("Update success", "success");
                    status = true;
                    return Json(status);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                SetAlert("Update Error", "error");
                ModelState.AddModelError("", ex.Message);
            }
            return Json(status);
        }
    }
}
