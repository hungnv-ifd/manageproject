﻿var project = {

    innit: function () {
        project.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var projectId = btn.data('id');
            $.ajax({
                url: "/ManageIProject/ChangeStatus",
                data: { projectId: projectId },
                dataType: "json",
                //contentType: "application/json;charset-utf-8",
                type:"POST",
                success: function (response) {
                    if (response.status) {
                        btn.text("Đã hoàn thành");
                    }
                    else {
                        btn.text("Chưa hoàn thành");
                    }
                }

            });
        })
    }


}
project.innit();