﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ManageProject.Startup))]
namespace ManageProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
